/** @file
 *	@brief MAVLink comm protocol testsuite generated from pixhawk_auto_mission.xml
 *	@see http://qgroundcontrol.org/mavlink/
 */
#ifndef PIXHAWK_AUTO_MISSION_TESTSUITE_H
#define PIXHAWK_AUTO_MISSION_TESTSUITE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MAVLINK_TEST_ALL
#define MAVLINK_TEST_ALL
static void mavlink_test_common(uint8_t, uint8_t, mavlink_message_t *last_msg);
static void mavlink_test_pixhawk_auto_mission(uint8_t, uint8_t, mavlink_message_t *last_msg);

static void mavlink_test_all(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_test_common(system_id, component_id, last_msg);
	mavlink_test_pixhawk_auto_mission(system_id, component_id, last_msg);
}
#endif

#include "../common/testsuite.h"


static void mavlink_test_set_cam_shutter(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_set_cam_shutter_t packet_in = {
		17.0,17443,17547,29,96,163
    };
	mavlink_set_cam_shutter_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.gain = packet_in.gain;
        	packet1.interval = packet_in.interval;
        	packet1.exposure = packet_in.exposure;
        	packet1.cam_no = packet_in.cam_no;
        	packet1.cam_mode = packet_in.cam_mode;
        	packet1.trigger_pin = packet_in.trigger_pin;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_set_cam_shutter_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_set_cam_shutter_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_set_cam_shutter_pack(system_id, component_id, &msg , packet1.cam_no , packet1.cam_mode , packet1.trigger_pin , packet1.interval , packet1.exposure , packet1.gain );
	mavlink_msg_set_cam_shutter_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_set_cam_shutter_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.cam_no , packet1.cam_mode , packet1.trigger_pin , packet1.interval , packet1.exposure , packet1.gain );
	mavlink_msg_set_cam_shutter_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_set_cam_shutter_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_set_cam_shutter_send(MAVLINK_COMM_1 , packet1.cam_no , packet1.cam_mode , packet1.trigger_pin , packet1.interval , packet1.exposure , packet1.gain );
	mavlink_msg_set_cam_shutter_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_image_triggered(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_image_triggered_t packet_in = {
		93372036854775807ULL,963497880,101.0,129.0,157.0,185.0,213.0,241.0,269.0,297.0,325.0,353.0
    };
	mavlink_image_triggered_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.timestamp = packet_in.timestamp;
        	packet1.seq = packet_in.seq;
        	packet1.roll = packet_in.roll;
        	packet1.pitch = packet_in.pitch;
        	packet1.yaw = packet_in.yaw;
        	packet1.local_z = packet_in.local_z;
        	packet1.lat = packet_in.lat;
        	packet1.lon = packet_in.lon;
        	packet1.alt = packet_in.alt;
        	packet1.ground_x = packet_in.ground_x;
        	packet1.ground_y = packet_in.ground_y;
        	packet1.ground_z = packet_in.ground_z;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_triggered_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_image_triggered_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_triggered_pack(system_id, component_id, &msg , packet1.timestamp , packet1.seq , packet1.roll , packet1.pitch , packet1.yaw , packet1.local_z , packet1.lat , packet1.lon , packet1.alt , packet1.ground_x , packet1.ground_y , packet1.ground_z );
	mavlink_msg_image_triggered_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_triggered_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.timestamp , packet1.seq , packet1.roll , packet1.pitch , packet1.yaw , packet1.local_z , packet1.lat , packet1.lon , packet1.alt , packet1.ground_x , packet1.ground_y , packet1.ground_z );
	mavlink_msg_image_triggered_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_image_triggered_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_triggered_send(MAVLINK_COMM_1 , packet1.timestamp , packet1.seq , packet1.roll , packet1.pitch , packet1.yaw , packet1.local_z , packet1.lat , packet1.lon , packet1.alt , packet1.ground_x , packet1.ground_y , packet1.ground_z );
	mavlink_msg_image_triggered_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_image_trigger_control(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_image_trigger_control_t packet_in = {
		5
    };
	mavlink_image_trigger_control_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.enable = packet_in.enable;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_trigger_control_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_image_trigger_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_trigger_control_pack(system_id, component_id, &msg , packet1.enable );
	mavlink_msg_image_trigger_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_trigger_control_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.enable );
	mavlink_msg_image_trigger_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_image_trigger_control_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_trigger_control_send(MAVLINK_COMM_1 , packet1.enable );
	mavlink_msg_image_trigger_control_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_image_available(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_image_available_t packet_in = {
		93372036854775807ULL,93372036854776311ULL,93372036854776815ULL,963498712,963498920,963499128,963499336,297.0,325.0,353.0,381.0,409.0,437.0,465.0,493.0,521.0,549.0,577.0,21603,21707,21811,147,214
    };
	mavlink_image_available_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.cam_id = packet_in.cam_id;
        	packet1.timestamp = packet_in.timestamp;
        	packet1.valid_until = packet_in.valid_until;
        	packet1.img_seq = packet_in.img_seq;
        	packet1.img_buf_index = packet_in.img_buf_index;
        	packet1.key = packet_in.key;
        	packet1.exposure = packet_in.exposure;
        	packet1.gain = packet_in.gain;
        	packet1.roll = packet_in.roll;
        	packet1.pitch = packet_in.pitch;
        	packet1.yaw = packet_in.yaw;
        	packet1.local_z = packet_in.local_z;
        	packet1.lat = packet_in.lat;
        	packet1.lon = packet_in.lon;
        	packet1.alt = packet_in.alt;
        	packet1.ground_x = packet_in.ground_x;
        	packet1.ground_y = packet_in.ground_y;
        	packet1.ground_z = packet_in.ground_z;
        	packet1.width = packet_in.width;
        	packet1.height = packet_in.height;
        	packet1.depth = packet_in.depth;
        	packet1.cam_no = packet_in.cam_no;
        	packet1.channels = packet_in.channels;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_available_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_image_available_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_available_pack(system_id, component_id, &msg , packet1.cam_id , packet1.cam_no , packet1.timestamp , packet1.valid_until , packet1.img_seq , packet1.img_buf_index , packet1.width , packet1.height , packet1.depth , packet1.channels , packet1.key , packet1.exposure , packet1.gain , packet1.roll , packet1.pitch , packet1.yaw , packet1.local_z , packet1.lat , packet1.lon , packet1.alt , packet1.ground_x , packet1.ground_y , packet1.ground_z );
	mavlink_msg_image_available_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_available_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.cam_id , packet1.cam_no , packet1.timestamp , packet1.valid_until , packet1.img_seq , packet1.img_buf_index , packet1.width , packet1.height , packet1.depth , packet1.channels , packet1.key , packet1.exposure , packet1.gain , packet1.roll , packet1.pitch , packet1.yaw , packet1.local_z , packet1.lat , packet1.lon , packet1.alt , packet1.ground_x , packet1.ground_y , packet1.ground_z );
	mavlink_msg_image_available_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_image_available_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_image_available_send(MAVLINK_COMM_1 , packet1.cam_id , packet1.cam_no , packet1.timestamp , packet1.valid_until , packet1.img_seq , packet1.img_buf_index , packet1.width , packet1.height , packet1.depth , packet1.channels , packet1.key , packet1.exposure , packet1.gain , packet1.roll , packet1.pitch , packet1.yaw , packet1.local_z , packet1.lat , packet1.lon , packet1.alt , packet1.ground_x , packet1.ground_y , packet1.ground_z );
	mavlink_msg_image_available_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_set_position_control_offset(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_set_position_control_offset_t packet_in = {
		17.0,45.0,73.0,101.0,53,120
    };
	mavlink_set_position_control_offset_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.x = packet_in.x;
        	packet1.y = packet_in.y;
        	packet1.z = packet_in.z;
        	packet1.yaw = packet_in.yaw;
        	packet1.target_system = packet_in.target_system;
        	packet1.target_component = packet_in.target_component;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_set_position_control_offset_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_set_position_control_offset_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_set_position_control_offset_pack(system_id, component_id, &msg , packet1.target_system , packet1.target_component , packet1.x , packet1.y , packet1.z , packet1.yaw );
	mavlink_msg_set_position_control_offset_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_set_position_control_offset_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.target_system , packet1.target_component , packet1.x , packet1.y , packet1.z , packet1.yaw );
	mavlink_msg_set_position_control_offset_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_set_position_control_offset_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_set_position_control_offset_send(MAVLINK_COMM_1 , packet1.target_system , packet1.target_component , packet1.x , packet1.y , packet1.z , packet1.yaw );
	mavlink_msg_set_position_control_offset_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_position_control_setpoint(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_position_control_setpoint_t packet_in = {
		17.0,45.0,73.0,101.0,18067
    };
	mavlink_position_control_setpoint_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.x = packet_in.x;
        	packet1.y = packet_in.y;
        	packet1.z = packet_in.z;
        	packet1.yaw = packet_in.yaw;
        	packet1.id = packet_in.id;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_position_control_setpoint_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_position_control_setpoint_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_position_control_setpoint_pack(system_id, component_id, &msg , packet1.id , packet1.x , packet1.y , packet1.z , packet1.yaw );
	mavlink_msg_position_control_setpoint_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_position_control_setpoint_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.id , packet1.x , packet1.y , packet1.z , packet1.yaw );
	mavlink_msg_position_control_setpoint_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_position_control_setpoint_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_position_control_setpoint_send(MAVLINK_COMM_1 , packet1.id , packet1.x , packet1.y , packet1.z , packet1.yaw );
	mavlink_msg_position_control_setpoint_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_marker(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_marker_t packet_in = {
		17.0,45.0,73.0,101.0,129.0,157.0,18483
    };
	mavlink_marker_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.x = packet_in.x;
        	packet1.y = packet_in.y;
        	packet1.z = packet_in.z;
        	packet1.roll = packet_in.roll;
        	packet1.pitch = packet_in.pitch;
        	packet1.yaw = packet_in.yaw;
        	packet1.id = packet_in.id;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_marker_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_marker_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_marker_pack(system_id, component_id, &msg , packet1.id , packet1.x , packet1.y , packet1.z , packet1.roll , packet1.pitch , packet1.yaw );
	mavlink_msg_marker_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_marker_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.id , packet1.x , packet1.y , packet1.z , packet1.roll , packet1.pitch , packet1.yaw );
	mavlink_msg_marker_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_marker_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_marker_send(MAVLINK_COMM_1 , packet1.id , packet1.x , packet1.y , packet1.z , packet1.roll , packet1.pitch , packet1.yaw );
	mavlink_msg_marker_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_raw_aux(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_raw_aux_t packet_in = {
		963497464,17443,17547,17651,17755,17859,17963
    };
	mavlink_raw_aux_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.baro = packet_in.baro;
        	packet1.adc1 = packet_in.adc1;
        	packet1.adc2 = packet_in.adc2;
        	packet1.adc3 = packet_in.adc3;
        	packet1.adc4 = packet_in.adc4;
        	packet1.vbat = packet_in.vbat;
        	packet1.temp = packet_in.temp;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_raw_aux_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_raw_aux_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_raw_aux_pack(system_id, component_id, &msg , packet1.adc1 , packet1.adc2 , packet1.adc3 , packet1.adc4 , packet1.vbat , packet1.temp , packet1.baro );
	mavlink_msg_raw_aux_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_raw_aux_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.adc1 , packet1.adc2 , packet1.adc3 , packet1.adc4 , packet1.vbat , packet1.temp , packet1.baro );
	mavlink_msg_raw_aux_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_raw_aux_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_raw_aux_send(MAVLINK_COMM_1 , packet1.adc1 , packet1.adc2 , packet1.adc3 , packet1.adc4 , packet1.vbat , packet1.temp , packet1.baro );
	mavlink_msg_raw_aux_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_watchdog_heartbeat(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_watchdog_heartbeat_t packet_in = {
		17235,17339
    };
	mavlink_watchdog_heartbeat_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.watchdog_id = packet_in.watchdog_id;
        	packet1.process_count = packet_in.process_count;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_heartbeat_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_watchdog_heartbeat_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_heartbeat_pack(system_id, component_id, &msg , packet1.watchdog_id , packet1.process_count );
	mavlink_msg_watchdog_heartbeat_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_heartbeat_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.watchdog_id , packet1.process_count );
	mavlink_msg_watchdog_heartbeat_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_watchdog_heartbeat_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_heartbeat_send(MAVLINK_COMM_1 , packet1.watchdog_id , packet1.process_count );
	mavlink_msg_watchdog_heartbeat_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_watchdog_process_info(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_watchdog_process_info_t packet_in = {
		963497464,17443,17547,"IJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABC","EFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRST"
    };
	mavlink_watchdog_process_info_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.timeout = packet_in.timeout;
        	packet1.watchdog_id = packet_in.watchdog_id;
        	packet1.process_id = packet_in.process_id;
        
        	mav_array_memcpy(packet1.name, packet_in.name, sizeof(char)*100);
        	mav_array_memcpy(packet1.arguments, packet_in.arguments, sizeof(char)*147);
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_process_info_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_watchdog_process_info_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_process_info_pack(system_id, component_id, &msg , packet1.watchdog_id , packet1.process_id , packet1.name , packet1.arguments , packet1.timeout );
	mavlink_msg_watchdog_process_info_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_process_info_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.watchdog_id , packet1.process_id , packet1.name , packet1.arguments , packet1.timeout );
	mavlink_msg_watchdog_process_info_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_watchdog_process_info_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_process_info_send(MAVLINK_COMM_1 , packet1.watchdog_id , packet1.process_id , packet1.name , packet1.arguments , packet1.timeout );
	mavlink_msg_watchdog_process_info_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_watchdog_process_status(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_watchdog_process_status_t packet_in = {
		963497464,17443,17547,17651,163,230
    };
	mavlink_watchdog_process_status_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.pid = packet_in.pid;
        	packet1.watchdog_id = packet_in.watchdog_id;
        	packet1.process_id = packet_in.process_id;
        	packet1.crashes = packet_in.crashes;
        	packet1.state = packet_in.state;
        	packet1.muted = packet_in.muted;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_process_status_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_watchdog_process_status_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_process_status_pack(system_id, component_id, &msg , packet1.watchdog_id , packet1.process_id , packet1.state , packet1.muted , packet1.pid , packet1.crashes );
	mavlink_msg_watchdog_process_status_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_process_status_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.watchdog_id , packet1.process_id , packet1.state , packet1.muted , packet1.pid , packet1.crashes );
	mavlink_msg_watchdog_process_status_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_watchdog_process_status_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_process_status_send(MAVLINK_COMM_1 , packet1.watchdog_id , packet1.process_id , packet1.state , packet1.muted , packet1.pid , packet1.crashes );
	mavlink_msg_watchdog_process_status_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_watchdog_command(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_watchdog_command_t packet_in = {
		17235,17339,17,84
    };
	mavlink_watchdog_command_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.watchdog_id = packet_in.watchdog_id;
        	packet1.process_id = packet_in.process_id;
        	packet1.target_system_id = packet_in.target_system_id;
        	packet1.command_id = packet_in.command_id;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_command_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_watchdog_command_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_command_pack(system_id, component_id, &msg , packet1.target_system_id , packet1.watchdog_id , packet1.process_id , packet1.command_id );
	mavlink_msg_watchdog_command_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_command_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.target_system_id , packet1.watchdog_id , packet1.process_id , packet1.command_id );
	mavlink_msg_watchdog_command_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_watchdog_command_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_watchdog_command_send(MAVLINK_COMM_1 , packet1.target_system_id , packet1.watchdog_id , packet1.process_id , packet1.command_id );
	mavlink_msg_watchdog_command_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_pattern_detected(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_pattern_detected_t packet_in = {
		17.0,17,"FGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ",128
    };
	mavlink_pattern_detected_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.confidence = packet_in.confidence;
        	packet1.type = packet_in.type;
        	packet1.detected = packet_in.detected;
        
        	mav_array_memcpy(packet1.file, packet_in.file, sizeof(char)*100);
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_pattern_detected_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_pattern_detected_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_pattern_detected_pack(system_id, component_id, &msg , packet1.type , packet1.confidence , packet1.file , packet1.detected );
	mavlink_msg_pattern_detected_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_pattern_detected_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.type , packet1.confidence , packet1.file , packet1.detected );
	mavlink_msg_pattern_detected_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_pattern_detected_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_pattern_detected_send(MAVLINK_COMM_1 , packet1.type , packet1.confidence , packet1.file , packet1.detected );
	mavlink_msg_pattern_detected_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_point_of_interest(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_point_of_interest_t packet_in = {
		17.0,45.0,73.0,17859,175,242,53,"RSTUVWXYZABCDEFGHIJKLMNOP"
    };
	mavlink_point_of_interest_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.x = packet_in.x;
        	packet1.y = packet_in.y;
        	packet1.z = packet_in.z;
        	packet1.timeout = packet_in.timeout;
        	packet1.type = packet_in.type;
        	packet1.color = packet_in.color;
        	packet1.coordinate_system = packet_in.coordinate_system;
        
        	mav_array_memcpy(packet1.name, packet_in.name, sizeof(char)*26);
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_point_of_interest_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_point_of_interest_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_point_of_interest_pack(system_id, component_id, &msg , packet1.type , packet1.color , packet1.coordinate_system , packet1.timeout , packet1.x , packet1.y , packet1.z , packet1.name );
	mavlink_msg_point_of_interest_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_point_of_interest_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.type , packet1.color , packet1.coordinate_system , packet1.timeout , packet1.x , packet1.y , packet1.z , packet1.name );
	mavlink_msg_point_of_interest_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_point_of_interest_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_point_of_interest_send(MAVLINK_COMM_1 , packet1.type , packet1.color , packet1.coordinate_system , packet1.timeout , packet1.x , packet1.y , packet1.z , packet1.name );
	mavlink_msg_point_of_interest_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_point_of_interest_connection(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_point_of_interest_connection_t packet_in = {
		17.0,45.0,73.0,101.0,129.0,157.0,18483,211,22,89,"DEFGHIJKLMNOPQRSTUVWXYZAB"
    };
	mavlink_point_of_interest_connection_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.xp1 = packet_in.xp1;
        	packet1.yp1 = packet_in.yp1;
        	packet1.zp1 = packet_in.zp1;
        	packet1.xp2 = packet_in.xp2;
        	packet1.yp2 = packet_in.yp2;
        	packet1.zp2 = packet_in.zp2;
        	packet1.timeout = packet_in.timeout;
        	packet1.type = packet_in.type;
        	packet1.color = packet_in.color;
        	packet1.coordinate_system = packet_in.coordinate_system;
        
        	mav_array_memcpy(packet1.name, packet_in.name, sizeof(char)*26);
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_point_of_interest_connection_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_point_of_interest_connection_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_point_of_interest_connection_pack(system_id, component_id, &msg , packet1.type , packet1.color , packet1.coordinate_system , packet1.timeout , packet1.xp1 , packet1.yp1 , packet1.zp1 , packet1.xp2 , packet1.yp2 , packet1.zp2 , packet1.name );
	mavlink_msg_point_of_interest_connection_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_point_of_interest_connection_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.type , packet1.color , packet1.coordinate_system , packet1.timeout , packet1.xp1 , packet1.yp1 , packet1.zp1 , packet1.xp2 , packet1.yp2 , packet1.zp2 , packet1.name );
	mavlink_msg_point_of_interest_connection_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_point_of_interest_connection_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_point_of_interest_connection_send(MAVLINK_COMM_1 , packet1.type , packet1.color , packet1.coordinate_system , packet1.timeout , packet1.xp1 , packet1.yp1 , packet1.zp1 , packet1.xp2 , packet1.yp2 , packet1.zp2 , packet1.name );
	mavlink_msg_point_of_interest_connection_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_brief_feature(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_brief_feature_t packet_in = {
		17.0,45.0,73.0,101.0,18067,18171,65,{ 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163 }
    };
	mavlink_brief_feature_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.x = packet_in.x;
        	packet1.y = packet_in.y;
        	packet1.z = packet_in.z;
        	packet1.response = packet_in.response;
        	packet1.size = packet_in.size;
        	packet1.orientation = packet_in.orientation;
        	packet1.orientation_assignment = packet_in.orientation_assignment;
        
        	mav_array_memcpy(packet1.descriptor, packet_in.descriptor, sizeof(uint8_t)*32);
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_brief_feature_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_brief_feature_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_brief_feature_pack(system_id, component_id, &msg , packet1.x , packet1.y , packet1.z , packet1.orientation_assignment , packet1.size , packet1.orientation , packet1.descriptor , packet1.response );
	mavlink_msg_brief_feature_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_brief_feature_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.x , packet1.y , packet1.z , packet1.orientation_assignment , packet1.size , packet1.orientation , packet1.descriptor , packet1.response );
	mavlink_msg_brief_feature_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_brief_feature_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_brief_feature_send(MAVLINK_COMM_1 , packet1.x , packet1.y , packet1.z , packet1.orientation_assignment , packet1.size , packet1.orientation , packet1.descriptor , packet1.response );
	mavlink_msg_brief_feature_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_attitude_control(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_attitude_control_t packet_in = {
		17.0,45.0,73.0,101.0,53,120,187,254,65
    };
	mavlink_attitude_control_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.roll = packet_in.roll;
        	packet1.pitch = packet_in.pitch;
        	packet1.yaw = packet_in.yaw;
        	packet1.thrust = packet_in.thrust;
        	packet1.target = packet_in.target;
        	packet1.roll_manual = packet_in.roll_manual;
        	packet1.pitch_manual = packet_in.pitch_manual;
        	packet1.yaw_manual = packet_in.yaw_manual;
        	packet1.thrust_manual = packet_in.thrust_manual;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_attitude_control_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_attitude_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_attitude_control_pack(system_id, component_id, &msg , packet1.target , packet1.roll , packet1.pitch , packet1.yaw , packet1.thrust , packet1.roll_manual , packet1.pitch_manual , packet1.yaw_manual , packet1.thrust_manual );
	mavlink_msg_attitude_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_attitude_control_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.target , packet1.roll , packet1.pitch , packet1.yaw , packet1.thrust , packet1.roll_manual , packet1.pitch_manual , packet1.yaw_manual , packet1.thrust_manual );
	mavlink_msg_attitude_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_attitude_control_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_attitude_control_send(MAVLINK_COMM_1 , packet1.target , packet1.roll , packet1.pitch , packet1.yaw , packet1.thrust , packet1.roll_manual , packet1.pitch_manual , packet1.yaw_manual , packet1.thrust_manual );
	mavlink_msg_attitude_control_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_detection_stats(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_detection_stats_t packet_in = {
		963497464,963497672,73.0,963498088,963498296,963498504,963498712,963498920,963499128,963499336,963499544,325.0
    };
	mavlink_detection_stats_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.detections = packet_in.detections;
        	packet1.cluster_iters = packet_in.cluster_iters;
        	packet1.best_score = packet_in.best_score;
        	packet1.best_lat = packet_in.best_lat;
        	packet1.best_lon = packet_in.best_lon;
        	packet1.best_alt = packet_in.best_alt;
        	packet1.best_detection_id = packet_in.best_detection_id;
        	packet1.best_cluster_id = packet_in.best_cluster_id;
        	packet1.best_cluster_iter_id = packet_in.best_cluster_iter_id;
        	packet1.images_done = packet_in.images_done;
        	packet1.images_todo = packet_in.images_todo;
        	packet1.fps = packet_in.fps;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_detection_stats_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_detection_stats_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_detection_stats_pack(system_id, component_id, &msg , packet1.detections , packet1.cluster_iters , packet1.best_score , packet1.best_lat , packet1.best_lon , packet1.best_alt , packet1.best_detection_id , packet1.best_cluster_id , packet1.best_cluster_iter_id , packet1.images_done , packet1.images_todo , packet1.fps );
	mavlink_msg_detection_stats_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_detection_stats_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.detections , packet1.cluster_iters , packet1.best_score , packet1.best_lat , packet1.best_lon , packet1.best_alt , packet1.best_detection_id , packet1.best_cluster_id , packet1.best_cluster_iter_id , packet1.images_done , packet1.images_todo , packet1.fps );
	mavlink_msg_detection_stats_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_detection_stats_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_detection_stats_send(MAVLINK_COMM_1 , packet1.detections , packet1.cluster_iters , packet1.best_score , packet1.best_lat , packet1.best_lon , packet1.best_alt , packet1.best_detection_id , packet1.best_cluster_id , packet1.best_cluster_iter_id , packet1.images_done , packet1.images_todo , packet1.fps );
	mavlink_msg_detection_stats_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_onboard_health(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_onboard_health_t packet_in = {
		963497464,45.0,73.0,101.0,129.0,157.0,185.0,213.0,18899,235,46,113,180,247
    };
	mavlink_onboard_health_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.uptime = packet_in.uptime;
        	packet1.ram_total = packet_in.ram_total;
        	packet1.swap_total = packet_in.swap_total;
        	packet1.disk_total = packet_in.disk_total;
        	packet1.temp = packet_in.temp;
        	packet1.voltage = packet_in.voltage;
        	packet1.network_load_in = packet_in.network_load_in;
        	packet1.network_load_out = packet_in.network_load_out;
        	packet1.cpu_freq = packet_in.cpu_freq;
        	packet1.cpu_load = packet_in.cpu_load;
        	packet1.ram_usage = packet_in.ram_usage;
        	packet1.swap_usage = packet_in.swap_usage;
        	packet1.disk_health = packet_in.disk_health;
        	packet1.disk_usage = packet_in.disk_usage;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_onboard_health_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_onboard_health_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_onboard_health_pack(system_id, component_id, &msg , packet1.uptime , packet1.cpu_freq , packet1.cpu_load , packet1.ram_usage , packet1.ram_total , packet1.swap_usage , packet1.swap_total , packet1.disk_health , packet1.disk_usage , packet1.disk_total , packet1.temp , packet1.voltage , packet1.network_load_in , packet1.network_load_out );
	mavlink_msg_onboard_health_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_onboard_health_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.uptime , packet1.cpu_freq , packet1.cpu_load , packet1.ram_usage , packet1.ram_total , packet1.swap_usage , packet1.swap_total , packet1.disk_health , packet1.disk_usage , packet1.disk_total , packet1.temp , packet1.voltage , packet1.network_load_in , packet1.network_load_out );
	mavlink_msg_onboard_health_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_onboard_health_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_onboard_health_send(MAVLINK_COMM_1 , packet1.uptime , packet1.cpu_freq , packet1.cpu_load , packet1.ram_usage , packet1.ram_total , packet1.swap_usage , packet1.swap_total , packet1.disk_health , packet1.disk_usage , packet1.disk_total , packet1.temp , packet1.voltage , packet1.network_load_in , packet1.network_load_out );
	mavlink_msg_onboard_health_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_gps_sia(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_gps_sia_t packet_in = {
		93372036854775807ULL,963497880,963498088,129.0,157.0,185.0,213.0,101,168
    };
	mavlink_gps_sia_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.timestamp = packet_in.timestamp;
        	packet1.gps_jingdu = packet_in.gps_jingdu;
        	packet1.gps_weidu = packet_in.gps_weidu;
        	packet1.gps_haiba = packet_in.gps_haiba;
        	packet1.gps_bei = packet_in.gps_bei;
        	packet1.gps_dong = packet_in.gps_dong;
        	packet1.gps_di = packet_in.gps_di;
        	packet1.gps_weixing = packet_in.gps_weixing;
        	packet1.gps_dingwei = packet_in.gps_dingwei;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_gps_sia_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_gps_sia_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_gps_sia_pack(system_id, component_id, &msg , packet1.timestamp , packet1.gps_jingdu , packet1.gps_weidu , packet1.gps_haiba , packet1.gps_bei , packet1.gps_dong , packet1.gps_di , packet1.gps_weixing , packet1.gps_dingwei );
	mavlink_msg_gps_sia_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_gps_sia_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.timestamp , packet1.gps_jingdu , packet1.gps_weidu , packet1.gps_haiba , packet1.gps_bei , packet1.gps_dong , packet1.gps_di , packet1.gps_weixing , packet1.gps_dingwei );
	mavlink_msg_gps_sia_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_gps_sia_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_gps_sia_send(MAVLINK_COMM_1 , packet1.timestamp , packet1.gps_jingdu , packet1.gps_weidu , packet1.gps_haiba , packet1.gps_bei , packet1.gps_dong , packet1.gps_di , packet1.gps_weixing , packet1.gps_dingwei );
	mavlink_msg_gps_sia_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_mission_buckets_position(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_mission_buckets_position_t packet_in = {
		123.0,179.0,235.0,291.0,347.0,403.0,459.0,515.0,571.0,627.0,683.0,739.0,795.0,851.0,907.0,963.0,1019.0,1075.0,1131.0,1187.0,1137.0,1165.0,1193.0,1221.0,1249.0,1277.0,1305.0,1333.0,1361.0,1389.0
    };
	mavlink_mission_buckets_position_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.bucketA_lat_curr = packet_in.bucketA_lat_curr;
        	packet1.bucketA_lon_curr = packet_in.bucketA_lon_curr;
        	packet1.bucketA_lat_next = packet_in.bucketA_lat_next;
        	packet1.bucketA_lon_next = packet_in.bucketA_lon_next;
        	packet1.bucketB_lat_curr = packet_in.bucketB_lat_curr;
        	packet1.bucketB_lon_curr = packet_in.bucketB_lon_curr;
        	packet1.bucketB_lat_next = packet_in.bucketB_lat_next;
        	packet1.bucketB_lon_next = packet_in.bucketB_lon_next;
        	packet1.bucketC_lat_curr = packet_in.bucketC_lat_curr;
        	packet1.bucketC_lon_curr = packet_in.bucketC_lon_curr;
        	packet1.bucketC_lat_next = packet_in.bucketC_lat_next;
        	packet1.bucketC_lon_next = packet_in.bucketC_lon_next;
        	packet1.bucketD_lat_curr = packet_in.bucketD_lat_curr;
        	packet1.bucketD_lon_curr = packet_in.bucketD_lon_curr;
        	packet1.bucketD_lat_next = packet_in.bucketD_lat_next;
        	packet1.bucketD_lon_next = packet_in.bucketD_lon_next;
        	packet1.copter_lat_home = packet_in.copter_lat_home;
        	packet1.copter_lon_home = packet_in.copter_lon_home;
        	packet1.copter_lat_guide = packet_in.copter_lat_guide;
        	packet1.copter_lon_guide = packet_in.copter_lon_guide;
        	packet1.bucketA_alt_curr = packet_in.bucketA_alt_curr;
        	packet1.bucketA_alt_next = packet_in.bucketA_alt_next;
        	packet1.bucketB_alt_curr = packet_in.bucketB_alt_curr;
        	packet1.bucketB_alt_next = packet_in.bucketB_alt_next;
        	packet1.bucketC_alt_curr = packet_in.bucketC_alt_curr;
        	packet1.bucketC_alt_next = packet_in.bucketC_alt_next;
        	packet1.bucketD_alt_curr = packet_in.bucketD_alt_curr;
        	packet1.bucketD_alt_next = packet_in.bucketD_alt_next;
        	packet1.copter_alt_home = packet_in.copter_alt_home;
        	packet1.copter_alt_guide = packet_in.copter_alt_guide;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_buckets_position_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_mission_buckets_position_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_buckets_position_pack(system_id, component_id, &msg , packet1.bucketA_lat_curr , packet1.bucketA_lon_curr , packet1.bucketA_alt_curr , packet1.bucketA_lat_next , packet1.bucketA_lon_next , packet1.bucketA_alt_next , packet1.bucketB_lat_curr , packet1.bucketB_lon_curr , packet1.bucketB_alt_curr , packet1.bucketB_lat_next , packet1.bucketB_lon_next , packet1.bucketB_alt_next , packet1.bucketC_lat_curr , packet1.bucketC_lon_curr , packet1.bucketC_alt_curr , packet1.bucketC_lat_next , packet1.bucketC_lon_next , packet1.bucketC_alt_next , packet1.bucketD_lat_curr , packet1.bucketD_lon_curr , packet1.bucketD_alt_curr , packet1.bucketD_lat_next , packet1.bucketD_lon_next , packet1.bucketD_alt_next , packet1.copter_lat_home , packet1.copter_lon_home , packet1.copter_alt_home , packet1.copter_lat_guide , packet1.copter_lon_guide , packet1.copter_alt_guide );
	mavlink_msg_mission_buckets_position_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_buckets_position_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.bucketA_lat_curr , packet1.bucketA_lon_curr , packet1.bucketA_alt_curr , packet1.bucketA_lat_next , packet1.bucketA_lon_next , packet1.bucketA_alt_next , packet1.bucketB_lat_curr , packet1.bucketB_lon_curr , packet1.bucketB_alt_curr , packet1.bucketB_lat_next , packet1.bucketB_lon_next , packet1.bucketB_alt_next , packet1.bucketC_lat_curr , packet1.bucketC_lon_curr , packet1.bucketC_alt_curr , packet1.bucketC_lat_next , packet1.bucketC_lon_next , packet1.bucketC_alt_next , packet1.bucketD_lat_curr , packet1.bucketD_lon_curr , packet1.bucketD_alt_curr , packet1.bucketD_lat_next , packet1.bucketD_lon_next , packet1.bucketD_alt_next , packet1.copter_lat_home , packet1.copter_lon_home , packet1.copter_alt_home , packet1.copter_lat_guide , packet1.copter_lon_guide , packet1.copter_alt_guide );
	mavlink_msg_mission_buckets_position_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_mission_buckets_position_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_buckets_position_send(MAVLINK_COMM_1 , packet1.bucketA_lat_curr , packet1.bucketA_lon_curr , packet1.bucketA_alt_curr , packet1.bucketA_lat_next , packet1.bucketA_lon_next , packet1.bucketA_alt_next , packet1.bucketB_lat_curr , packet1.bucketB_lon_curr , packet1.bucketB_alt_curr , packet1.bucketB_lat_next , packet1.bucketB_lon_next , packet1.bucketB_alt_next , packet1.bucketC_lat_curr , packet1.bucketC_lon_curr , packet1.bucketC_alt_curr , packet1.bucketC_lat_next , packet1.bucketC_lon_next , packet1.bucketC_alt_next , packet1.bucketD_lat_curr , packet1.bucketD_lon_curr , packet1.bucketD_alt_curr , packet1.bucketD_lat_next , packet1.bucketD_lon_next , packet1.bucketD_alt_next , packet1.copter_lat_home , packet1.copter_lon_home , packet1.copter_alt_home , packet1.copter_lat_guide , packet1.copter_lon_guide , packet1.copter_alt_guide );
	mavlink_msg_mission_buckets_position_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_mission_pos_poll(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_mission_pos_poll_t packet_in = {
		5
    };
	mavlink_mission_pos_poll_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.poll_buckets = packet_in.poll_buckets;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_pos_poll_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_mission_pos_poll_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_pos_poll_pack(system_id, component_id, &msg , packet1.poll_buckets );
	mavlink_msg_mission_pos_poll_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_pos_poll_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.poll_buckets );
	mavlink_msg_mission_pos_poll_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_mission_pos_poll_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_pos_poll_send(MAVLINK_COMM_1 , packet1.poll_buckets );
	mavlink_msg_mission_pos_poll_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_mission_start_stop(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_mission_start_stop_t packet_in = {
		5,72,139
    };
	mavlink_mission_start_stop_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.start_flag = packet_in.start_flag;
        	packet1.main_commander = packet_in.main_commander;
        	packet1.sub_commander = packet_in.sub_commander;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_start_stop_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_mission_start_stop_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_start_stop_pack(system_id, component_id, &msg , packet1.start_flag , packet1.main_commander , packet1.sub_commander );
	mavlink_msg_mission_start_stop_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_start_stop_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.start_flag , packet1.main_commander , packet1.sub_commander );
	mavlink_msg_mission_start_stop_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_mission_start_stop_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_mission_start_stop_send(MAVLINK_COMM_1 , packet1.start_flag , packet1.main_commander , packet1.sub_commander );
	mavlink_msg_mission_start_stop_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_vision_command(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_vision_command_t packet_in = {
		5
    };
	mavlink_vision_command_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.vision_cmd = packet_in.vision_cmd;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_vision_command_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_vision_command_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_vision_command_pack(system_id, component_id, &msg , packet1.vision_cmd );
	mavlink_msg_vision_command_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_vision_command_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.vision_cmd );
	mavlink_msg_vision_command_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_vision_command_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_vision_command_send(MAVLINK_COMM_1 , packet1.vision_cmd );
	mavlink_msg_vision_command_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_vision_circle_all(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_vision_circle_all_t packet_in = {
		17.0,45.0,73.0,101.0,129.0,157.0,185.0,213.0,241.0,269.0,297.0,325.0,353.0,381.0,409.0,437.0,465.0,493.0,521.0,549.0,577.0,605.0,633.0,661.0,37,104,171,238,49,116,183,250,61,128,195
    };
	mavlink_vision_circle_all_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.circle_0_x = packet_in.circle_0_x;
        	packet1.circle_0_y = packet_in.circle_0_y;
        	packet1.circle_0_z = packet_in.circle_0_z;
        	packet1.circle_1_x = packet_in.circle_1_x;
        	packet1.circle_1_y = packet_in.circle_1_y;
        	packet1.circle_1_z = packet_in.circle_1_z;
        	packet1.circle_2_x = packet_in.circle_2_x;
        	packet1.circle_2_y = packet_in.circle_2_y;
        	packet1.circle_2_z = packet_in.circle_2_z;
        	packet1.circle_3_x = packet_in.circle_3_x;
        	packet1.circle_3_y = packet_in.circle_3_y;
        	packet1.circle_3_z = packet_in.circle_3_z;
        	packet1.circle_4_x = packet_in.circle_4_x;
        	packet1.circle_4_y = packet_in.circle_4_y;
        	packet1.circle_4_z = packet_in.circle_4_z;
        	packet1.circle_5_x = packet_in.circle_5_x;
        	packet1.circle_5_y = packet_in.circle_5_y;
        	packet1.circle_5_z = packet_in.circle_5_z;
        	packet1.circle_6_x = packet_in.circle_6_x;
        	packet1.circle_6_y = packet_in.circle_6_y;
        	packet1.circle_6_z = packet_in.circle_6_z;
        	packet1.circle_7_x = packet_in.circle_7_x;
        	packet1.circle_7_y = packet_in.circle_7_y;
        	packet1.circle_7_z = packet_in.circle_7_z;
        	packet1.circle_sum = packet_in.circle_sum;
        	packet1.marker_sum = packet_in.marker_sum;
        	packet1.number_mode = packet_in.number_mode;
        	packet1.circle_0_number = packet_in.circle_0_number;
        	packet1.circle_1_number = packet_in.circle_1_number;
        	packet1.circle_2_number = packet_in.circle_2_number;
        	packet1.circle_3_number = packet_in.circle_3_number;
        	packet1.circle_4_number = packet_in.circle_4_number;
        	packet1.circle_5_number = packet_in.circle_5_number;
        	packet1.circle_6_number = packet_in.circle_6_number;
        	packet1.circle_7_number = packet_in.circle_7_number;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_vision_circle_all_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_vision_circle_all_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_vision_circle_all_pack(system_id, component_id, &msg , packet1.circle_sum , packet1.marker_sum , packet1.number_mode , packet1.circle_0_number , packet1.circle_0_x , packet1.circle_0_y , packet1.circle_0_z , packet1.circle_1_number , packet1.circle_1_x , packet1.circle_1_y , packet1.circle_1_z , packet1.circle_2_number , packet1.circle_2_x , packet1.circle_2_y , packet1.circle_2_z , packet1.circle_3_number , packet1.circle_3_x , packet1.circle_3_y , packet1.circle_3_z , packet1.circle_4_number , packet1.circle_4_x , packet1.circle_4_y , packet1.circle_4_z , packet1.circle_5_number , packet1.circle_5_x , packet1.circle_5_y , packet1.circle_5_z , packet1.circle_6_number , packet1.circle_6_x , packet1.circle_6_y , packet1.circle_6_z , packet1.circle_7_number , packet1.circle_7_x , packet1.circle_7_y , packet1.circle_7_z );
	mavlink_msg_vision_circle_all_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_vision_circle_all_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.circle_sum , packet1.marker_sum , packet1.number_mode , packet1.circle_0_number , packet1.circle_0_x , packet1.circle_0_y , packet1.circle_0_z , packet1.circle_1_number , packet1.circle_1_x , packet1.circle_1_y , packet1.circle_1_z , packet1.circle_2_number , packet1.circle_2_x , packet1.circle_2_y , packet1.circle_2_z , packet1.circle_3_number , packet1.circle_3_x , packet1.circle_3_y , packet1.circle_3_z , packet1.circle_4_number , packet1.circle_4_x , packet1.circle_4_y , packet1.circle_4_z , packet1.circle_5_number , packet1.circle_5_x , packet1.circle_5_y , packet1.circle_5_z , packet1.circle_6_number , packet1.circle_6_x , packet1.circle_6_y , packet1.circle_6_z , packet1.circle_7_number , packet1.circle_7_x , packet1.circle_7_y , packet1.circle_7_z );
	mavlink_msg_vision_circle_all_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_vision_circle_all_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_vision_circle_all_send(MAVLINK_COMM_1 , packet1.circle_sum , packet1.marker_sum , packet1.number_mode , packet1.circle_0_number , packet1.circle_0_x , packet1.circle_0_y , packet1.circle_0_z , packet1.circle_1_number , packet1.circle_1_x , packet1.circle_1_y , packet1.circle_1_z , packet1.circle_2_number , packet1.circle_2_x , packet1.circle_2_y , packet1.circle_2_z , packet1.circle_3_number , packet1.circle_3_x , packet1.circle_3_y , packet1.circle_3_z , packet1.circle_4_number , packet1.circle_4_x , packet1.circle_4_y , packet1.circle_4_z , packet1.circle_5_number , packet1.circle_5_x , packet1.circle_5_y , packet1.circle_5_z , packet1.circle_6_number , packet1.circle_6_x , packet1.circle_6_y , packet1.circle_6_z , packet1.circle_7_number , packet1.circle_7_x , packet1.circle_7_y , packet1.circle_7_z );
	mavlink_msg_vision_circle_all_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_hc_buckets_poll(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_hc_buckets_poll_t packet_in = {
		123.0,179.0,235.0,291.0,347.0,403.0,459.0,515.0,571.0,627.0,683.0,739.0,795.0,851.0,907.0,963.0,1019.0,1075.0,1131.0,1187.0,1137.0,1165.0,1193.0,1221.0,1249.0,1277.0,1305.0,1333.0,1361.0,1389.0
    };
	mavlink_hc_buckets_poll_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.bucketA_lat_curr = packet_in.bucketA_lat_curr;
        	packet1.bucketA_lon_curr = packet_in.bucketA_lon_curr;
        	packet1.bucketA_lat_next = packet_in.bucketA_lat_next;
        	packet1.bucketA_lon_next = packet_in.bucketA_lon_next;
        	packet1.bucketB_lat_curr = packet_in.bucketB_lat_curr;
        	packet1.bucketB_lon_curr = packet_in.bucketB_lon_curr;
        	packet1.bucketB_lat_next = packet_in.bucketB_lat_next;
        	packet1.bucketB_lon_next = packet_in.bucketB_lon_next;
        	packet1.bucketC_lat_curr = packet_in.bucketC_lat_curr;
        	packet1.bucketC_lon_curr = packet_in.bucketC_lon_curr;
        	packet1.bucketC_lat_next = packet_in.bucketC_lat_next;
        	packet1.bucketC_lon_next = packet_in.bucketC_lon_next;
        	packet1.bucketD_lat_curr = packet_in.bucketD_lat_curr;
        	packet1.bucketD_lon_curr = packet_in.bucketD_lon_curr;
        	packet1.bucketD_lat_next = packet_in.bucketD_lat_next;
        	packet1.bucketD_lon_next = packet_in.bucketD_lon_next;
        	packet1.lat_home = packet_in.lat_home;
        	packet1.lon_home = packet_in.lon_home;
        	packet1.lat_guide = packet_in.lat_guide;
        	packet1.lon_guide = packet_in.lon_guide;
        	packet1.bucketA_alt_curr = packet_in.bucketA_alt_curr;
        	packet1.bucketA_alt_next = packet_in.bucketA_alt_next;
        	packet1.bucketB_alt_curr = packet_in.bucketB_alt_curr;
        	packet1.bucketB_alt_next = packet_in.bucketB_alt_next;
        	packet1.bucketC_alt_curr = packet_in.bucketC_alt_curr;
        	packet1.bucketC_alt_next = packet_in.bucketC_alt_next;
        	packet1.bucketD_alt_curr = packet_in.bucketD_alt_curr;
        	packet1.bucketD_alt_next = packet_in.bucketD_alt_next;
        	packet1.alt_home = packet_in.alt_home;
        	packet1.alt_guide = packet_in.alt_guide;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_hc_buckets_poll_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_hc_buckets_poll_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_hc_buckets_poll_pack(system_id, component_id, &msg , packet1.bucketA_lat_curr , packet1.bucketA_lon_curr , packet1.bucketA_alt_curr , packet1.bucketA_lat_next , packet1.bucketA_lon_next , packet1.bucketA_alt_next , packet1.bucketB_lat_curr , packet1.bucketB_lon_curr , packet1.bucketB_alt_curr , packet1.bucketB_lat_next , packet1.bucketB_lon_next , packet1.bucketB_alt_next , packet1.bucketC_lat_curr , packet1.bucketC_lon_curr , packet1.bucketC_alt_curr , packet1.bucketC_lat_next , packet1.bucketC_lon_next , packet1.bucketC_alt_next , packet1.bucketD_lat_curr , packet1.bucketD_lon_curr , packet1.bucketD_alt_curr , packet1.bucketD_lat_next , packet1.bucketD_lon_next , packet1.bucketD_alt_next , packet1.lat_home , packet1.lon_home , packet1.alt_home , packet1.lat_guide , packet1.lon_guide , packet1.alt_guide );
	mavlink_msg_hc_buckets_poll_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_hc_buckets_poll_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.bucketA_lat_curr , packet1.bucketA_lon_curr , packet1.bucketA_alt_curr , packet1.bucketA_lat_next , packet1.bucketA_lon_next , packet1.bucketA_alt_next , packet1.bucketB_lat_curr , packet1.bucketB_lon_curr , packet1.bucketB_alt_curr , packet1.bucketB_lat_next , packet1.bucketB_lon_next , packet1.bucketB_alt_next , packet1.bucketC_lat_curr , packet1.bucketC_lon_curr , packet1.bucketC_alt_curr , packet1.bucketC_lat_next , packet1.bucketC_lon_next , packet1.bucketC_alt_next , packet1.bucketD_lat_curr , packet1.bucketD_lon_curr , packet1.bucketD_alt_curr , packet1.bucketD_lat_next , packet1.bucketD_lon_next , packet1.bucketD_alt_next , packet1.lat_home , packet1.lon_home , packet1.alt_home , packet1.lat_guide , packet1.lon_guide , packet1.alt_guide );
	mavlink_msg_hc_buckets_poll_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_hc_buckets_poll_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_hc_buckets_poll_send(MAVLINK_COMM_1 , packet1.bucketA_lat_curr , packet1.bucketA_lon_curr , packet1.bucketA_alt_curr , packet1.bucketA_lat_next , packet1.bucketA_lon_next , packet1.bucketA_alt_next , packet1.bucketB_lat_curr , packet1.bucketB_lon_curr , packet1.bucketB_alt_curr , packet1.bucketB_lat_next , packet1.bucketB_lon_next , packet1.bucketB_alt_next , packet1.bucketC_lat_curr , packet1.bucketC_lon_curr , packet1.bucketC_alt_curr , packet1.bucketC_lat_next , packet1.bucketC_lon_next , packet1.bucketC_alt_next , packet1.bucketD_lat_curr , packet1.bucketD_lon_curr , packet1.bucketD_alt_curr , packet1.bucketD_lat_next , packet1.bucketD_lon_next , packet1.bucketD_alt_next , packet1.lat_home , packet1.lon_home , packet1.alt_home , packet1.lat_guide , packet1.lon_guide , packet1.alt_guide );
	mavlink_msg_hc_buckets_poll_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_hc_state_status(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_hc_state_status_t packet_in = {
		93372036854775807ULL,73.0,101.0,129.0,157.0,185.0,213.0,101,168,235,46,113,180,247
    };
	mavlink_hc_state_status_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.timestamp = packet_in.timestamp;
        	packet1.pos_sp_x = packet_in.pos_sp_x;
        	packet1.pos_sp_y = packet_in.pos_sp_y;
        	packet1.pos_sp_z = packet_in.pos_sp_z;
        	packet1.vel_sp_x = packet_in.vel_sp_x;
        	packet1.vel_sp_y = packet_in.vel_sp_y;
        	packet1.vel_sp_z = packet_in.vel_sp_z;
        	packet1.buckets_vaild = packet_in.buckets_vaild;
        	packet1.cmd_recevied = packet_in.cmd_recevied;
        	packet1.poll_recevied = packet_in.poll_recevied;
        	packet1.main_state = packet_in.main_state;
        	packet1.sec_state = packet_in.sec_state;
        	packet1.pos_en = packet_in.pos_en;
        	packet1.vel_en = packet_in.vel_en;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_hc_state_status_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_hc_state_status_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_hc_state_status_pack(system_id, component_id, &msg , packet1.timestamp , packet1.buckets_vaild , packet1.cmd_recevied , packet1.poll_recevied , packet1.main_state , packet1.sec_state , packet1.pos_sp_x , packet1.pos_sp_y , packet1.pos_sp_z , packet1.vel_sp_x , packet1.vel_sp_y , packet1.vel_sp_z , packet1.pos_en , packet1.vel_en );
	mavlink_msg_hc_state_status_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_hc_state_status_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.timestamp , packet1.buckets_vaild , packet1.cmd_recevied , packet1.poll_recevied , packet1.main_state , packet1.sec_state , packet1.pos_sp_x , packet1.pos_sp_y , packet1.pos_sp_z , packet1.vel_sp_x , packet1.vel_sp_y , packet1.vel_sp_z , packet1.pos_en , packet1.vel_en );
	mavlink_msg_hc_state_status_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_hc_state_status_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_hc_state_status_send(MAVLINK_COMM_1 , packet1.timestamp , packet1.buckets_vaild , packet1.cmd_recevied , packet1.poll_recevied , packet1.main_state , packet1.sec_state , packet1.pos_sp_x , packet1.pos_sp_y , packet1.pos_sp_z , packet1.vel_sp_x , packet1.vel_sp_y , packet1.vel_sp_z , packet1.pos_en , packet1.vel_en );
	mavlink_msg_hc_state_status_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_pixhawk_auto_mission(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_test_set_cam_shutter(system_id, component_id, last_msg);
	mavlink_test_image_triggered(system_id, component_id, last_msg);
	mavlink_test_image_trigger_control(system_id, component_id, last_msg);
	mavlink_test_image_available(system_id, component_id, last_msg);
	mavlink_test_set_position_control_offset(system_id, component_id, last_msg);
	mavlink_test_position_control_setpoint(system_id, component_id, last_msg);
	mavlink_test_marker(system_id, component_id, last_msg);
	mavlink_test_raw_aux(system_id, component_id, last_msg);
	mavlink_test_watchdog_heartbeat(system_id, component_id, last_msg);
	mavlink_test_watchdog_process_info(system_id, component_id, last_msg);
	mavlink_test_watchdog_process_status(system_id, component_id, last_msg);
	mavlink_test_watchdog_command(system_id, component_id, last_msg);
	mavlink_test_pattern_detected(system_id, component_id, last_msg);
	mavlink_test_point_of_interest(system_id, component_id, last_msg);
	mavlink_test_point_of_interest_connection(system_id, component_id, last_msg);
	mavlink_test_brief_feature(system_id, component_id, last_msg);
	mavlink_test_attitude_control(system_id, component_id, last_msg);
	mavlink_test_detection_stats(system_id, component_id, last_msg);
	mavlink_test_onboard_health(system_id, component_id, last_msg);
	mavlink_test_gps_sia(system_id, component_id, last_msg);
	mavlink_test_mission_buckets_position(system_id, component_id, last_msg);
	mavlink_test_mission_pos_poll(system_id, component_id, last_msg);
	mavlink_test_mission_start_stop(system_id, component_id, last_msg);
	mavlink_test_vision_command(system_id, component_id, last_msg);
	mavlink_test_vision_circle_all(system_id, component_id, last_msg);
	mavlink_test_hc_buckets_poll(system_id, component_id, last_msg);
	mavlink_test_hc_state_status(system_id, component_id, last_msg);
}

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // PIXHAWK_AUTO_MISSION_TESTSUITE_H
