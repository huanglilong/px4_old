#
# add by huanglilong -- 2015-9-24
#

MODULE_COMMAND	= hc_state_send

SRCS		= hc_state_send.c

# Startup handler, the actual app stack size is
# in the task_spawn command
MODULE_STACKSIZE = 1200
