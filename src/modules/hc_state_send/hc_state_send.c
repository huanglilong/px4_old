/**
 * file 	: hc_state_send.c
 * author	: huanglilong
 * time		: 2015-9-24
 * brief	: send state machine state
 */

#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>
#include <drivers/drv_hrt.h>
#include <arch/board/board.h>

#include <uORB/uORB.h>
#include <uORB/topics/mission_buckets_position.h>
#include <uORB/topics/mission_pos_poll.h>
#include <uORB/topics/mission_start_stop.h>
#include <uORB/topics/hc_buckets_poll.h>
#include <uORB/topics/hc_state_status.h>
#include <uORB/topics/plan_guidance.h>
#include <nuttx/config.h>

#include <stdlib.h>
#include <nuttx/sched.h>
#include <systemlib/systemlib.h>
#include <systemlib/err.h>


static bool thread_should_exit = false;		/**< hc_state_send exit flag */
static bool thread_running = false;			/**< hc_state_send status flag */
static int  hc_state_send_task;				/**< Handle of hc_state_send task / thread */

__EXPORT int hc_state_send_main(int argc, char *argv[]);

/**
 * Mainloop of hc_state_send
 */
int hc_state_send_thread(int argc, char *argv[]);

/**
 * print the correct usage for hc_state_send operating
 */
static void usage(const char *reason);

static void
usage(const char *reason)
{
	if(reason)
		warnx("%s\n", reason);
	errx(1,"usage: hc_state_send {start|stop|status} [-p <additional params>]\n\n");
}
/**
 * hc_state_send
 */
int hc_state_send_main(int argc, char *argv[])
{
	if (argc < 1)
			usage("missing command");

		if (!strcmp(argv[1], "start")) {

			if (thread_running) {
				warnx("hc_state_send already running\n");
				/* this is not an error */
				exit(0);
			}
			thread_should_exit = false;
			hc_state_send_task = task_spawn_cmd("hc_state_send",
											SCHED_DEFAULT,
											SCHED_PRIORITY_DEFAULT,
											1200,
											hc_state_send_thread,
											(argv) ? (char * const *)&argv[2] : (char * const *)NULL
											);
			exit(0);
		}
		if (!strcmp(argv[1], "stop")) {
			thread_should_exit = true;
			exit(0);
		}

		if (!strcmp(argv[1], "status")) {
			if (thread_running) {
				warnx("\trunning\n");
			} else {
				warnx("\tnot started\n");
			}
			exit(0);
		}

		usage("unrecognized command");
		exit(1);

}

/**
 * hc_state_send
 */
int hc_state_send_thread(int argc, char *argv[])
{
	int pos_poll_sub        = orb_subscribe(ORB_ID(mission_pos_poll));			/* poll receive */
	int start_stop_sub      = orb_subscribe(ORB_ID(mission_start_stop));		/* start cmd */
	int buckets_pos_sub     = orb_subscribe(ORB_ID(mission_buckets_position));	/* buckets_vaild */
	int plan_guide_sub      = orb_subscribe(ORB_ID(plan_guidance));				/* state machine status */

	struct hc_state_status_s hc_state_status_data;
	memset(&hc_state_status_data, 0, sizeof(hc_state_status_data));

	orb_advert_t hc_state_status_pub = orb_advertise(ORB_ID(hc_state_status), &hc_state_status_data);

	struct pollfd fds[4] = {
		{ .fd = pos_poll_sub,          .events = POLLIN },
		{ .fd = start_stop_sub,        .events = POLLIN },
		{ .fd = buckets_pos_sub,       .events = POLLIN },
		{ .fd = plan_guide_sub,        .events = POLLIN },
		};

	thread_running = true;
	while(!thread_should_exit)
	{

		int pret = poll(&fds[0], (sizeof(fds) / sizeof(fds[0])), 1000);
		/* publisher state */
		if (pret > 0)
		{

			if (fds[0].revents & POLLIN)
			{
				struct mission_pos_poll_s pos_poll;
				orb_copy(ORB_ID(mission_pos_poll), pos_poll_sub, &pos_poll);
				hc_state_status_data.poll_recevied = pos_poll.poll_buckets;		/* just for feedback */

			}
			if (fds[1].revents & POLLIN)
			{
				struct mission_start_stop_s msg_cmd;
				orb_copy(ORB_ID(mission_start_stop), start_stop_sub, &msg_cmd);
				hc_state_status_data.cmd_recevied = msg_cmd.start_flag;			/* receive GCS cmd */
				hc_state_status_data.main_state   = msg_cmd.main_commander;
				hc_state_status_data.sec_state    = msg_cmd.sub_commander;

			}
			if (fds[2].revents & POLLIN)
			{
				struct mission_buckets_position_s bucket_pos;
				orb_copy(ORB_ID(mission_buckets_position), buckets_pos_sub, &bucket_pos);
				hc_state_status_data.buckets_vaild = true;
			}
			if (fds[3].revents & POLLIN)
			{
				struct plan_guidance_s msg_guide;
				orb_copy(ORB_ID(plan_guidance), plan_guide_sub, &msg_guide);	/* feedback state machine status to GCS */
				hc_state_status_data.main_state = msg_guide.Main_State;
				hc_state_status_data.sec_state  = msg_guide.Sec_State;
				hc_state_status_data.pos_sp_x  = msg_guide.x;
				hc_state_status_data.pos_sp_y  = msg_guide.y;
				hc_state_status_data.pos_sp_z  = msg_guide.z;
				hc_state_status_data.vel_sp_x  = msg_guide.XSpeed;
				hc_state_status_data.vel_sp_y  = msg_guide.YSpeed;
				hc_state_status_data.vel_sp_z  = msg_guide.ZSpeed;
				hc_state_status_data.pos_en	   = msg_guide.Pos_En;
				hc_state_status_data.vel_en    = msg_guide.Vel_En;
			}
			/* publisher state */
			if (hc_state_status_pub > 0) {
				orb_publish(ORB_ID(hc_state_status), hc_state_status_pub, &hc_state_status_data);

			} else {
				hc_state_status_pub = orb_advertise(ORB_ID(hc_state_status), &hc_state_status_data);
			}
		}
		hc_state_status_data.timestamp = hrt_absolute_time();
		if (hc_state_status_pub > 0) {
			orb_publish(ORB_ID(hc_state_status), hc_state_status_pub, &hc_state_status_data);
		} else {
			hc_state_status_pub = orb_advertise(ORB_ID(hc_state_status), &hc_state_status_data);
		}
	}
	thread_running = false;
	return 0;
}
