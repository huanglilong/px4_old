#
# Build heilcopter bucket catch controller
#

MODULE_COMMAND	= hc_bucket_catch_control

SRCS		= hc_bucket_catch_control_main.cpp \
			  hc_bucket_catch_control_params.c

# Startup handler, the actual app stack size is
# in the task_spawn command
MODULE_STACKSIZE = 1200
