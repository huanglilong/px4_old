/**
 * file 	: hc_bucket_catch_control_main.cpp
 * brief	: Helicopter bucket catch controller.
 * author	: huanglilong(huanglilongwk@163.com)
 * time		: 2015/9/14
 * ref		: mc_att_control_main.cpp
 */

#include <px4.h>
#include <functional>
#include <cstdio>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <drivers/drv_hrt.h>
#include <arch/board/board.h>

#include <uORB/uORB.h>
#include <uORB/topics/actuator_controls.h>
#include <uORB/topics/manual_control_setpoint.h>
#include <uORB/topics/plan_guidance.h>

#include <systemlib/param/param.h>
#include <systemlib/err.h>
#include <systemlib/perf_counter.h>
#include <systemlib/systemlib.h>
#include <systemlib/circuit_breaker.h>
#include <lib/mathlib/mathlib.h>

#define	HOOK_OPEN  	 1.0f
#define HOOK_CLOSE  -1.0f

extern "C" __EXPORT int hc_bucket_catch_control_main(int argc, char *argv[]);

class HelicopterBucketCatchControl
{
public:
	/**
	 * Constructor
	 */
	HelicopterBucketCatchControl();

	/**
	 * Destructor, also kills the main task
	 */
	~HelicopterBucketCatchControl();

	/**
	 * Start the Helicopter bucket catch control task.
	 *
	 * @return		OK on success.
	 */
	int		start();

private:

	bool	_task_should_exit;						/**< if true, task_main() should exit */
	int		_control_task;							/**< task handle */

	int 	_params_sub;							/* wait for params updates */
	int		_manual_control_aux1_sub;				/**< manual control aux1 subscription */
	int     _task_plan_sub;							/**< catch control sign */
	orb_id_t _actuators_id;							/**< pointer to correct actuator controls0 uORB metadata structure */
	orb_advert_t _actuators_1_pub;					/**< bucket catch actuator controls publication */

	struct actuator_controls_s			_actuators;				/**< actuator controls */
	struct manual_control_setpoint_s	_manual_control_bucket;	/**< manual control setpoint */
	struct plan_guidance_s				_task_catch_ctl;		/**< control sign from task plan */
	struct {
		param_t actutor_output;						/* actuator output */
	}		_params_handles;						/**< handles for interesting parameters */

	/**
	 * Shim for calling task_main from task_create.
	 */
	static void	task_main_trampoline(int argc, char *argv[]);

	/**
	 * Main attitude control task.
	 */
	void		task_main();
};

namespace hc_bucket_catch_control
{

/* oddly, ERROR is not defined for c++ */
#ifdef ERROR
# undef ERROR
#endif
static const int ERROR = -1;

HelicopterBucketCatchControl	*g_control;
}
HelicopterBucketCatchControl::HelicopterBucketCatchControl() :

	_task_should_exit(false),
	_control_task(-1),
	_params_sub(-1),
	_manual_control_aux1_sub(-1),
	_task_plan_sub(-1),
	_actuators_id(0),
	_actuators_1_pub(-1)

{
	memset(&_actuators, 0, sizeof(_actuators));
	memset(&_manual_control_bucket, 0, sizeof(_manual_control_bucket));
	memset(&_task_catch_ctl, 0, sizeof(_task_catch_ctl));
	_params_handles.actutor_output = param_find("HBC_ACTUTOR_OUT");
}

HelicopterBucketCatchControl::~HelicopterBucketCatchControl()
{
	if (_control_task != -1) {
		/* task wakes up every 100ms or so at the longest */
		_task_should_exit = true;

		/* wait for a second for the task to quit at our request */
		unsigned i = 0;

		do {
			/* wait 20ms */
			usleep(20000);

			/* if we have given up, kill it */
			if (++i > 50) {
				task_delete(_control_task);
				break;
			}
		} while (_control_task != -1);
	}

	hc_bucket_catch_control::g_control = nullptr;
}
int
HelicopterBucketCatchControl::start()
{
	ASSERT(_control_task == -1);

	/* start the task */
	_control_task = task_spawn_cmd("hc_bucket_catch_control",
				       SCHED_DEFAULT,
				       SCHED_PRIORITY_MAX - 5,
				       1500,
				       (main_t)&HelicopterBucketCatchControl::task_main_trampoline,
				       nullptr);

	if (_control_task < 0) {
		warn("task start failed");
		return -errno;
	}

	return OK;
}

void
HelicopterBucketCatchControl::task_main_trampoline(int argc, char *argv[])
{
	hc_bucket_catch_control::g_control->task_main();
}

void
HelicopterBucketCatchControl::task_main()
{

	_manual_control_aux1_sub = orb_subscribe(ORB_ID(manual_control_setpoint));
	_task_plan_sub           = orb_subscribe(ORB_ID(plan_guidance));

	struct pollfd fds[2];
	fds[0].fd = _manual_control_aux1_sub;
	fds[0].events = POLLIN;
	fds[1].fd = _task_plan_sub;
	fds[1].events = POLLIN;

	_actuators_id = ORB_ID(actuator_controls_1);

	while (!_task_should_exit) {

		/* wait for up to 100ms for data */
		int pret = poll(&fds[0], (sizeof(fds) / sizeof(fds[0])), 200);

		/* timed out - periodic check for _task_should_exit */
		if (pret == 0)
			continue;

		/* this is undesirable but not much we can do - might want to flag unhappy status */
		if (pret < 0) {
			warn("poll error %d, %d", pret, errno);
			/* sleep a bit before next try */
			usleep(100000);
			continue;
		}
		if (fds[0].revents & POLLIN){
			orb_copy(ORB_ID(manual_control_setpoint), _manual_control_aux1_sub, &_manual_control_bucket);
			_actuators.control[0] = _manual_control_bucket.aux1;			/* update the actutor output */
			_actuators.control[1] = _manual_control_bucket.aux1;
			_actuators.control[2] = _manual_control_bucket.aux1;
			_actuators.control[3] = _manual_control_bucket.aux1;
			_actuators.timestamp = hrt_absolute_time();
			_actuators.timestamp_sample = _actuators.timestamp;
		}
		if (fds[1].revents & POLLIN){
			orb_copy(ORB_ID(plan_guidance), _task_plan_sub, &_task_catch_ctl);
			if(_task_catch_ctl.Hook_Ctrl){
				_actuators.control[0] = HOOK_OPEN;			/* update the actutor output */
				_actuators.control[1] = HOOK_OPEN;
				_actuators.control[2] = HOOK_OPEN;
				_actuators.control[3] = HOOK_OPEN;
				_actuators.timestamp = hrt_absolute_time();
				_actuators.timestamp_sample = _actuators.timestamp;
			}else{
				_actuators.control[0] = HOOK_CLOSE;			/* update the actutor output */
				_actuators.control[1] = HOOK_CLOSE;
				_actuators.control[2] = HOOK_CLOSE;
				_actuators.control[3] = HOOK_CLOSE;
				_actuators.timestamp = hrt_absolute_time();
				_actuators.timestamp_sample = _actuators.timestamp;
			}
		}
		if (_actuators_1_pub > 0) {
			orb_publish(_actuators_id, _actuators_1_pub, &_actuators);
		} else if (_actuators_id) {
			_actuators_1_pub = orb_advertise(_actuators_id, &_actuators);
		}
	}
}
int hc_bucket_catch_control_main(int argc, char *argv[])
{
	if (argc < 2) {
		errx(1, "usage: hc_bucket_catch_control_main {start|stop|status}");
	}

	if (!strcmp(argv[1], "start")) {

		if (hc_bucket_catch_control::g_control != nullptr)
			errx(1, "already running");

		hc_bucket_catch_control::g_control = new HelicopterBucketCatchControl;

		if (hc_bucket_catch_control::g_control == nullptr)
			errx(1, "alloc failed");

		if (OK != hc_bucket_catch_control::g_control->start()) {
			delete hc_bucket_catch_control::g_control;
			hc_bucket_catch_control::g_control = nullptr;
			err(1, "start failed");
		}

		exit(0);
	}

	if (!strcmp(argv[1], "stop")) {
		if (hc_bucket_catch_control::g_control == nullptr)
			errx(1, "not running");

		delete hc_bucket_catch_control::g_control;
		hc_bucket_catch_control::g_control = nullptr;
		exit(0);
	}

	if (!strcmp(argv[1], "status")) {
		if (hc_bucket_catch_control::g_control) {
			errx(0, "running");

		} else {
			errx(1, "not running");
		}
	}

	warnx("unrecognized command");
	return 1;
}

