/**
 * file 	: hc_bucket_catch_control_params.c
 * brief	: Helicopter bucket catch params.
 * author	: huanglilong(huanglilongwk@163.com)
 * time		: 2015/9/14
 */

#include <systemlib/param/param.h>

/**
 *  ACTUTOR OUTPUT FOR DEBUG
 *
 * @min 0.0
 * @max 1.0
 * @group Helicopter Control
 */
PARAM_DEFINE_FLOAT(HBC_ACTUTOR_OUT, 0.1f);
