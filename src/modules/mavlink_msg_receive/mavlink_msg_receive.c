/**
 * @file mavlink_msg_receive.c
 * test for my codes -- jom
 * 2015/4/6
 * note:
 * 这里两个函数orb_set_interval(sensor_sub_fd, 1000)和poll_ret = poll(fds, 1, 1000)容易混淆.
 * orb_set_interval设置代理的数据更新标志位的更新最小时间间隔, 期间可能代理数据更新了多次, 但是数据更新标志位可能因为最小时间
 * 间隔没达到而没置位.
 * poll根据代理的数据更新标志位来确定数据是否更新了.
 */

#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>

#include <uORB/uORB.h>
#include <uORB/topics/mission_buckets_position.h>			/* 包含 uORB的定义*/
#include <uORB/topics/mission_pos_poll.h>					/* 包含 uORB的定义*/
#include <uORB/topics/mission_start_stop.h>					/* 包含 uORB的定义*/
#include <uORB/topics/vision_command.h>						/* 包含 uORB的定义*/
#include <uORB/topics/vision_circle_all.h>					/* 包含 uORB的定义*/
#include <uORB/topics/gps_sia.h>
#include <uORB/topics/hc_buckets_poll.h>
#include <uORB/topics/hc_state_status.h>
#include <nuttx/config.h>

#include <stdlib.h>
#include <nuttx/sched.h>
#include <systemlib/systemlib.h>
#include <systemlib/err.h>


static bool thread_should_exit = false;		/**< mavlink_msg_receive exit flag */
static bool thread_running = false;			/**< mavlink_msg_receive status flag */
static int  mavlink_msg_receive_task;		/**< Handle of mavlink_msg_receive task / thread */

// 线程管理程序
__EXPORT int mavlink_msg_receive_main(int argc, char *argv[]);

/**
 * Mainloop of mavlink_msg_receive
 * 用户线程, 执行用户代码
 */
int mavlink_msg_receive_thread(int argc, char *argv[]);

/**
 * print the correct usage for mavlink_msg_receive operating
 */
static void usage(const char *reason);

static void
usage(const char *reason)
{
	if(reason)
		warnx("%s\n", reason);
	errx(1,"usage: mavlink_msg_receive {start|stop|status} [-p <additional params>]\n\n");
}
/**
 * mavlink_msg_receive模块的前台管理程序, 由shell启动, 可以控制模块的线程的启动和停止.
 */
int mavlink_msg_receive_main(int argc, char *argv[])
{
	if (argc < 1)
			usage("missing command");

		if (!strcmp(argv[1], "start")) {   //shell启动命令

			if (thread_running) {		   // 如果线程已经启动了
				warnx("mavlink_msg_receive already running\n");
				/* this is not an error */
				exit(0);
			}
			thread_should_exit = false;		// 将线程状态位设置为false
			// 创建线程, ucos非常相同.
			mavlink_msg_receive_task = task_spawn_cmd("mavlink_msg_receive",// 线程名
											SCHED_DEFAULT,					// 调度模式, 相同优先级的任务按先进先出(FIFO),不同优先级的任务按优先级
											SCHED_PRIORITY_DEFAULT,			// 优先级
											1200,							// 堆栈大小
											mavlink_msg_receive_thread,			// 线程入口
											(argv) ? (char * const *)&argv[2] : (char * const *)NULL // 线程参数
											);
			exit(0);						// 正常退出
		}
		if (!strcmp(argv[1], "stop")) {		// shell停止命令
			thread_should_exit = true;
			exit(0);
		}

		if (!strcmp(argv[1], "status")) {	// shell查询命令, 用于查询线程的状态.
			if (thread_running) {
				warnx("\trunning\n");
			} else {
				warnx("\tnot started\n");
			}
			exit(0);
		}

		usage("unrecognized command");
		exit(1);

}

/**
 * mavlink_msg_receive的后台线程, 用于执行用户任务
 */
int mavlink_msg_receive_thread(int argc, char *argv[])
{
	int sub_fd = orb_subscribe(ORB_ID(hc_buckets_poll));			// 订阅传感器数据
	int sub_fd_2 = orb_subscribe(ORB_ID(hc_state_status));			// 订阅传感器数据

	orb_set_interval(sub_fd, 1000);								// 设置topic(bus)数据读取最小时间间隔
	orb_set_interval(sub_fd_2, 1000);							// 设置topic(bus)数据读取最小时间间隔

	struct pollfd fds[] = {
		{ .fd = sub_fd,   .events = POLLIN },
		{ .fd = sub_fd_2,   .events = POLLIN }};

	thread_running = true;
	while(!thread_should_exit)				// 如果线程没有被停止
	{
		int poll_ret = poll(fds, 2, 1000);	// 线程等待数据更新, timeout为1s,
											// 如果数据没更新, 那么此时系统会进行任务切换
		if (poll_ret > 0)					// 没有数据更新
		{
			if (fds[0].revents & POLLIN)
			{
				struct hc_buckets_poll_s msg_data;
				orb_copy(ORB_ID(hc_buckets_poll), sub_fd, &msg_data);	// 拷贝数据
				printf("[mavlink_msg_receive] hc_buckets_poll:\t%8.4f\n",
					(double)msg_data.alt_home);

			}
			if (fds[1].revents & POLLIN)
			{
				struct hc_state_status_s msg_data_2;
				orb_copy(ORB_ID(hc_state_status), sub_fd_2, &msg_data_2);	// 拷贝数据
				printf("[mavlink_msg_receive] hc_state_status :\t%8.4f\t%8.4f\t%8.4f\n",
					(double)msg_data_2.buckets_vaild,(double)msg_data_2.main_state,(double)msg_data_2.sec_state);

			}
		}

	}
	thread_running = false;
	return 0;
}
