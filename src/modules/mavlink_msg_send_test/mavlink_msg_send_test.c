/**
 * file		: mavlink_msg_send_test.c
 * author	: jom
 * brief	: for mavlink msg test, this thread just publisher msg
 * time		: 2015/4/29
 */

#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>

#include <uORB/uORB.h>
#include <uORB/topics/mission_buckets_position.h>			/* 包含 uORB的定义*/
#include <uORB/topics/mission_pos_poll.h>					/* 包含 uORB的定义*/
#include <uORB/topics/mission_start_stop.h>					/* 包含 uORB的定义*/
#include <uORB/topics/vision_command.h>						/* 包含 uORB的定义*/
#include <uORB/topics/vehicle_global_position.h>			/* 包含 uORB的定义*/
#include <uORB/topics/vision_circle_all.h>					/* 包含 uORB的定义*/
#include <uORB/topics/gps_sia.h>
#include <uORB/topics/hc_buckets_poll.h>
#include <uORB/topics/hc_state_status.h>

#include <nuttx/config.h>

#include <stdlib.h>
#include <nuttx/sched.h>
#include <systemlib/systemlib.h>
#include <systemlib/err.h>


static bool thread_should_exit = false;				/* mavlink_msg_send_test exit flag */
static bool thread_running = false;					/* mavlink_msg_send_test status flag */
static int  mavlink_task;							/* Handle of mavlink_msg_send_test task / thread */

orb_advert_t hc_buckets_poll_handle;				/* handle for hc_buckets_poll topic */
orb_advert_t hc_state_status_handle;				/* handle for hc_buckets_poll topic */
orb_advert_t mission_start_stop_handle;				/* handle for mission_start_stop topic */

// 线程管理程序
__EXPORT int mavlink_msg_send_test_main(int argc, char *argv[]);

/**
 * Mainloop of mavlink_msg_send_test
 * 用户线程, 执行用户代码
 */
int mavlink_msg_send_test_thread(int argc, char *argv[]);

/**
 * print the correct usage for mavlink_msg_send_test operating
 */
static void usage(const char *reason);

static void
usage(const char *reason)
{
	if(reason)
		warnx("%s\n", reason);
	errx(1,"usage: mavlink_msg_send_test {start|stop|status} [-p <additional params>]\n\n");
}
/**
 * mavlink_msg_send_test模块的前台管理程序, 由shell启动, 可以控制模块的线程的启动和停止.
 */
int mavlink_msg_send_test_main(int argc, char *argv[])
{
	if (argc < 1)
			usage("missing command");

		if (!strcmp(argv[1], "start")) {   //shell启动命令

			if (thread_running) {		   // 如果线程已经启动了
				warnx("mavlink_msg_send_test already running\n");
				/* this is not an error */
				exit(0);
			}
			thread_should_exit = false;		// 将线程状态位设置为false
			mavlink_task = task_spawn_cmd("mavlink_msg_send_test",						// 线程名
											SCHED_DEFAULT,								// 调度模式, 相同优先级的任务按先进先出(FIFO),不同优先级的任务按优先级
											SCHED_PRIORITY_DEFAULT,						// 优先级
											1200,										// 堆栈大小
											mavlink_msg_send_test_thread,			// 线程入口
											(argv) ? (char * const *)&argv[2] : (char * const *)NULL // 线程参数
											);
			exit(0);						// 正常退出
		}
		if (!strcmp(argv[1], "stop")) {		// shell停止命令
			thread_should_exit = true;
			exit(0);
		}

		if (!strcmp(argv[1], "status")) {	// shell查询命令, 用于查询线程的状态.
			if (thread_running) {
				warnx("\trunning\n");
			} else {
				warnx("\tnot started\n");
			}
			exit(0);
		}

		usage("unrecognized command");
		exit(1);

}

/**
 * mavlink_msg_send_test的后台线程, 用于执行用户任务
 */
int mavlink_msg_send_test_thread(int argc, char *argv[])
{

	struct hc_buckets_poll_s hc_buckets_poll_data;
	hc_buckets_poll_handle = orb_advertise(ORB_ID(hc_buckets_poll), &hc_buckets_poll_data);

	struct hc_state_status_s hc_state_status_data;
	hc_state_status_handle = orb_advertise(ORB_ID(hc_state_status), &hc_state_status_data);

	struct mission_start_stop_s mission_start_stop_data;
	mission_start_stop_handle = orb_advertise(ORB_ID(mission_start_stop), &mission_start_stop_data);

	thread_running = true;
	while(!thread_should_exit)				// 如果线程没有被停止
	{
		struct hc_buckets_poll_s data1;
		memset(&data1, 0, sizeof(data1));
		data1.alt_home  = 10.1f;
		orb_publish(ORB_ID(hc_buckets_poll), hc_buckets_poll_handle, &data1);  // send data

		struct hc_state_status_s data2;
		memset(&data2, 0, sizeof(data2));
		data2.buckets_vaild = 2;
		orb_publish(ORB_ID(hc_state_status), hc_state_status_handle, &data2);  // send data

		struct mission_start_stop_s data3;
		memset(&data3, 0, sizeof(data3));
		data3.start_flag = true;
		data3.main_commander = 2;
		data3.sub_commander = 4;
		orb_publish(ORB_ID(mission_start_stop), mission_start_stop_handle, &data3);  // send data
		usleep(1000);
	}
	thread_running = false;
	return 0;
}
