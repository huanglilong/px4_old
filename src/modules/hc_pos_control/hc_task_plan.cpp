/**
 * file 	: hc_task_plan.cpp
 * brief	: Helicopter task planer -- implementation
 * author	: huanglilong(huanglilongwk@163.com)
 * time		: 2015/9/19
 * ref		: plan.c
 */
#include "hc_task_plan.h"

HelicopterTaskPlan::HelicopterTaskPlan(void):

	_state_main_curr(MAIN_STATE::M_IDLE),
	_state_sec_curr(SEC_STATE::S_IDLE),
	_buckets_valid(false),
	_catchTryCnts(0),
	_dt(0),
	_dt_sum(0),
	_manual_control(0),
	_arming(false),
	_isLocalBucketsInit(false),
	_take_off_flag(false),

	/* subscriber */
	_buckets_pos_sub(-1),
	_poll_pos_sub(-1),
	_start_stop_sub(-1),
	_gps_sia_sub(-1),

	/* publishers */
	_plan_guidance_pub(-1),
	_buckets_poll_pub(-1),	/* buckets_pos feedback to GCS */

	_ref_alt(0)				/* update from hc_pos */

{
	memset(&_plan_guide, 0, sizeof(_plan_guide));
	memset(&_mission_pos_poll, 0, sizeof(_mission_pos_poll));
	memset(&_mission_start_stop, 0, sizeof(_mission_start_stop));
	memset(&_mission_buckets_pos, 0, sizeof(_mission_buckets_pos));
	memset(&_guide_global_pos, 0, sizeof(_guide_global_pos));
	memset(&_buckets_poll, 0, sizeof(_buckets_poll));

	memset(&_pos_curr, 0, sizeof(_pos_curr));						/* update from hc_pos */
	memset(&_params, 0, sizeof(_params));							/* update from hc_pos */

	memset(&_bucket_a_curr, 0, sizeof(_bucket_a_curr));
	memset(&_bucket_b_curr, 0, sizeof(_bucket_b_curr));
	memset(&_bucket_c_curr, 0, sizeof(_bucket_d_curr));
	memset(&_bucket_d_curr, 0, sizeof(_bucket_d_curr));
	memset(&_bucket_a_next, 0, sizeof(_bucket_a_next));
	memset(&_bucket_b_next, 0, sizeof(_bucket_b_next));
	memset(&_bucket_c_next, 0, sizeof(_bucket_c_next));
	memset(&_bucket_d_next, 0, sizeof(_bucket_d_next));
	memset(&_hc_home, 0, sizeof(_hc_home));
	memset(&_hc_guide, 0, sizeof(_hc_guide));

	memset(&_d_pos_a_curr, 0, sizeof(_d_pos_a_curr));
	memset(&_d_pos_b_curr, 0, sizeof(_d_pos_b_curr));
	memset(&_d_pos_c_curr, 0, sizeof(_d_pos_c_curr));
	memset(&_d_pos_d_curr, 0, sizeof(_d_pos_d_curr));
	memset(&_d_pos_a_next, 0, sizeof(_d_pos_a_next));
	memset(&_d_pos_b_next, 0, sizeof(_d_pos_b_next));
	memset(&_d_pos_c_next, 0, sizeof(_d_pos_c_curr));
	memset(&_d_pos_d_next, 0, sizeof(_d_pos_d_next));
}

HelicopterTaskPlan::~HelicopterTaskPlan(void)
{

}
/**
 * poll all data
 */
void
HelicopterTaskPlan::poll_subscriptions(void)
{
	bool updated;
	orb_check(_buckets_pos_sub, &updated);		/* ckeck buckets data update */
	orb_copy(ORB_ID(mission_buckets_position), _buckets_pos_sub, &_mission_buckets_pos);
	if(updated){
		_buckets_valid = true;					/* bucket updated */
		_isLocalBucketsInit = false;			/* force to recalculate d_pos */
	}

	orb_check(_start_stop_sub, &updated);		/* check task start-stop flag */
	if (updated) {
		orb_copy(ORB_ID(mission_start_stop), _start_stop_sub, &_mission_start_stop);
		if(_mission_start_stop.start_flag == true){
			_state_main_curr = main_state_valid(_mission_start_stop.main_commander);
			_state_sec_curr  = sec_state_valid(_mission_start_stop.sub_commander);
		}
	}

	orb_check(_poll_pos_sub, &updated);			/* check buckets has being recieved */
	if (updated) {
		orb_copy(ORB_ID(mission_pos_poll), _poll_pos_sub, &_mission_pos_poll);
		publish_buckets_pos();					/* return buckets position */
	}
}
/**
 * task state machine use gps
 */
bool
HelicopterTaskPlan::task_state_machine_GPS(void)
{
	/* don't forget add subscribers in hc */
	_dt_sum += _dt;									/* for hook operate delay */
	if(_mission_start_stop.start_flag == false){ 	/* stop -- force main state into IDLE */
		_state_main_curr = MAIN_STATE::M_IDLE;		/* force into idle state */
	}

	bool guide_update = update_local_pos();						/* get buckets pos */
	/* task data updated */
	if(guide_update && _buckets_valid && _arming && _isLocalBucketsInit){
		_plan_guide.PosResetPrev = _plan_guide.PosResetFlag;	/* save prev flag */
		guide_flag_set();	    								/* init flags */
		switch(_state_main_curr){
			/*********************************** IDLE STATE **********************************/
			case MAIN_STATE::M_IDLE:
				 /* reset pos and stady here */
				_plan_guide.Hook_Ctrl    = false;		/* open hook */
				_plan_guide.PosResetFlag = true;	/* reset pos */
				if(_take_off_flag == 0){
					_plan_guide.XSpeed       = 0.0f;
					_plan_guide.YSpeed       = 0.0f;
					_plan_guide.ZSpeed       = 0.5f;
				}
				 break;
			/*********************************** TAKE OFF ************************************/
			case MAIN_STATE::M_TAKEOFF_TAKEOFF:		/* stay here */
				_plan_guide.Pos_En = false;			/* no pos controller */
				_plan_guide.x = _hc_home(0);		/* not use just for safe */
				_plan_guide.y = _hc_home(1);
				_plan_guide.z = _pos_curr(2);
				_plan_guide.XSpeed = 0.0f;
				_plan_guide.YSpeed = 0.0f;
				_plan_guide.ZSpeed = -0.5f;
				if(_hc_home(2) - _pos_curr(2) > 0.5f){
					_state_main_curr = MAIN_STATE::M_TAKEOFF_HEIGHT_FIX;	/* change state */
				}
				break;
			case MAIN_STATE::M_TAKEOFF_HEIGHT_FIX:
				_plan_guide.x = _hc_home(0);
				_plan_guide.y = _hc_home(1);
				_plan_guide.z = _hc_home(2) - _params.takeoff_height;
				if(_plan_guide.z - _pos_curr(2) > 0.0f){
					_state_main_curr = MAIN_STATE::M_STATE_BUCKET_A;		/* track bucket A */
					_state_sec_curr  = SEC_STATE::S_BUCKET_TRACK_SUPPLY;
				}
				break;
			/*********************************** BUCKET A ************************************/
			case MAIN_STATE::M_STATE_BUCKET_A:
				bucket_task(_bucket_a_curr, _bucket_a_next);
				break;	/* end of Main state(Bucket A task)*/
			/*********************************** BUCKET B ************************************/
			case MAIN_STATE::M_STATE_BUCKET_B:
				bucket_task(_bucket_b_curr, _bucket_b_next);
				break;	/* end of Main state(Bucket B task)*/
			/*********************************** BUCKET C ************************************/
			case MAIN_STATE::M_STATE_BUCKET_C:
				bucket_task(_bucket_c_curr, _bucket_c_next);
				break;	/* end of Main state(Bucket C task)*/
			/*********************************** BUCKET D ************************************/
			case MAIN_STATE::M_STATE_BUCKET_D:
				bucket_task(_bucket_d_curr, _bucket_d_next);
				break;	/* end of Main state(Bucket D task)*/
			case MAIN_STATE::M_STATE_LANDING_RETURN:
				_plan_guide.x = _hc_home(0);
				_plan_guide.y = _hc_home(1);
				_plan_guide.z = _hc_home(2) - _params.takeoff_height;
				_plan_guide.Hook_Ctrl = false;	/* open hook */
				if(fabsf(_plan_guide.x - _pos_curr(0)) < 1.0f * _params.bias_x &&
				   fabsf(_plan_guide.y - _pos_curr(1)) < 1.0f * _params.bias_y){
					_state_main_curr = MAIN_STATE::M_STATE_LANDING_LANDING;
				}
				break;
			case MAIN_STATE::M_STATE_LANDING_LANDING:
				_plan_guide.Pos_En = false;			/* not pos controller */
				_plan_guide.x = _pos_curr(0);       /* not use just for safe */
				_plan_guide.y = _pos_curr(1);
				_plan_guide.z = _pos_curr(2);
				_plan_guide.XSpeed = 0.0f;
				_plan_guide.YSpeed = 0.0f;
				_plan_guide.ZSpeed = 0.4f;
				_plan_guide.Hook_Ctrl = false;	/* open hook */
				if( _hc_home(2) - _pos_curr(2) < -0.1f){
					_state_main_curr = MAIN_STATE::M_STATE_TSAK_FINISHED;	/* change state */
				}
				break;
			case MAIN_STATE::M_STATE_TSAK_FINISHED:
				/* thrust set 0 */
				_plan_guide.Pos_En = false;			/* not pos controller */
				_plan_guide.x = _pos_curr(0);
				_plan_guide.y = _pos_curr(1);		/* not use just for safe */
				_plan_guide.z = _pos_curr(2);
				_plan_guide.XSpeed = 0.0f;
				_plan_guide.YSpeed = 0.0f;
				_plan_guide.ZSpeed = 0.4f;
				_plan_guide.Hook_Ctrl = false;			/* open hook */
				break;
			default:
				_state_main_curr = MAIN_STATE::M_IDLE;	/* no much to do */
				break;
		}
		_plan_guide.Main_State = _state_main_curr;
		_plan_guide.Sec_State  = _state_sec_curr;
		_plan_guide.PlanStepCnt = _catchTryCnts;
		if (_plan_guidance_pub > 0) {
			orb_publish(ORB_ID(plan_guidance), _plan_guidance_pub, &_plan_guide);
		} else {
			_plan_guidance_pub = orb_advertise(ORB_ID(plan_guidance), &_plan_guide);
		}
		return true;	/* guide data is avilable */
	} /* end of guide_update */
	else
		return false;	/* guide data is not avilable */
}
void
HelicopterTaskPlan::bucket_task(math::Vector<3> task_pos, math::Vector<3> task_pos_next)
{
	float hand_status = _manual_control;			/* debug -- use manual control */
	switch(_state_sec_curr){
		case SEC_STATE::S_BUCKET_TRACK_SUPPLY:
			_plan_guide.x = task_pos(0);
			_plan_guide.y = task_pos(1);
			_plan_guide.z = task_pos(2) - _params.track_height_low;	/* no supply */
			_plan_guide.Hook_Ctrl = false;							/* open hook */
			if(fabsf(_plan_guide.x - _pos_curr(0)) < 1.0f * _params.bias_x &&
			   fabsf(_plan_guide.y - _pos_curr(1)) < 1.0f * _params.bias_y){
				_state_sec_curr = SEC_STATE::S_BUCKET_PICK_FALL;
			}
			break;
		case SEC_STATE::S_BUCKET_PICK_FALL:
			_plan_guide.x = task_pos(0);
			_plan_guide.y = task_pos(1);
			_plan_guide.z = task_pos(2) - _params.catch_height;
			_plan_guide.Hook_Ctrl = false;							/* open hook */
			if(fabsf(_plan_guide.x - _pos_curr(0)) < _params.bias_x &&
			   fabsf(_plan_guide.y - _pos_curr(1)) < _params.bias_y &&
			   fabsf(_plan_guide.z - _pos_curr(2)) < _params.bias_z){
				_state_sec_curr = SEC_STATE::S_BUCKET_PICK_BUCKET;
				_dt_sum=0; /*next state */
			}
			break;
		case SEC_STATE::S_BUCKET_PICK_BUCKET:
			_plan_guide.x = task_pos(0);
			_plan_guide.y = task_pos(1);
			_plan_guide.z = task_pos(2) - _params.catch_height;
			_plan_guide.Hook_Ctrl = true;		/* close hook */
			if(_dt_sum > _params.hook_delay){	/* wait for 1s */
				_state_sec_curr = SEC_STATE::S_BUCKET_PICK_RISE;
				_catchTryCnts++;
				_dt_sum =0.0f;					/* for next time use */
			}
			break;
		case SEC_STATE::S_BUCKET_PICK_RISE:
			_plan_guide.x = task_pos(0);
			_plan_guide.y = task_pos(1);
			_plan_guide.z = task_pos(2) - _params.track_height_high;
			_plan_guide.Hook_Ctrl = true;		/* close hook */
			if(fabsf(_plan_guide.x - _pos_curr(0)) < 1.0f * _params.bias_x &&
			   fabsf(_plan_guide.y - _pos_curr(1)) < 1.0f * _params.bias_y &&
			        (_plan_guide.z - _pos_curr(2)) > 0.0f){
			   if(hand_status > 0.7f){
				   _state_sec_curr = SEC_STATE::S_BUCKET_TRACK_TARGET;
				   _catchTryCnts = 0;
			   }
			   else if(_catchTryCnts > _params.max_try){ /* give up and catch next bucket */
				   if(_state_main_curr != MAIN_STATE::M_STATE_BUCKET_D){ /* next bucket */
					   _state_main_curr++;
					   _state_sec_curr = SEC_STATE::S_BUCKET_TRACK_SUPPLY;
				   }
				   else{	/* should return home */
					   _state_main_curr = MAIN_STATE::M_STATE_LANDING_RETURN;
				   }
				   _catchTryCnts = 0;	/* clean for next state use */
			   }
			   else{ /* try again */
				   _state_sec_curr = SEC_STATE::S_BUCKET_PICK_FALL;
			   }
			}
			break;
		case SEC_STATE::S_BUCKET_TRACK_TARGET:
			_plan_guide.x = task_pos_next(0);
			_plan_guide.y = task_pos_next(1);
			_plan_guide.z = task_pos_next(2) - _params.track_height_high;
			_plan_guide.Hook_Ctrl = true;	/* close hook */
			if(fabsf(_plan_guide.x - _pos_curr(0)) < 1.0f * _params.bias_x &&
			   fabsf(_plan_guide.y - _pos_curr(1)) < 1.0f * _params.bias_y){
				_state_sec_curr = SEC_STATE::S_BUCKET_PUT_FALL;
			}
			break;
		case SEC_STATE::S_BUCKET_PUT_FALL:
			_plan_guide.x = task_pos_next(0);
			_plan_guide.y = task_pos_next(1);
			_plan_guide.z = task_pos_next(2) - _params.put_height;
			_plan_guide.Hook_Ctrl = true;	/* close hook */
			if(fabsf(_plan_guide.x - _pos_curr(0)) < _params.bias_x &&
			   fabsf(_plan_guide.y - _pos_curr(1)) < _params.bias_y &&
			   fabsf(_plan_guide.z - _pos_curr(2)) < _params.bias_z){
				_state_sec_curr = SEC_STATE::S_BUCKET_PUT_DOWN;
				_dt_sum =0;
			}
			break;
		case SEC_STATE::S_BUCKET_PUT_DOWN:
			_plan_guide.x = task_pos_next(0);
			_plan_guide.y = task_pos_next(1);
			_plan_guide.z = task_pos_next(2) - _params.put_height;
			_plan_guide.Hook_Ctrl = false;  	/* open */
			if(_dt_sum > _params.hook_delay){	/* wait for 2s */
				_state_sec_curr = SEC_STATE::S_BUCKET_PUT_RISE;
				_dt_sum = 0.0f;					/* for next time use */
			}
			break;
		case SEC_STATE::S_BUCKET_PUT_RISE:
			_plan_guide.x = task_pos_next(0);
			_plan_guide.y = task_pos_next(1);
			_plan_guide.z = task_pos_next(2) - _params.track_height_low;
			if(fabsf(_plan_guide.x - _pos_curr(0)) < 1.0f * _params.bias_x &&
			   fabsf(_plan_guide.y - _pos_curr(1)) < 1.0f * _params.bias_y &&
				(_plan_guide.z - _pos_curr(2)) > 0.0f){
			   if(_state_main_curr != MAIN_STATE::M_STATE_BUCKET_D){ /* next bucket */
				   _state_main_curr++;
				   _state_sec_curr = SEC_STATE::S_BUCKET_TRACK_SUPPLY;
			   }
			   else{	/* should return home */
				   _state_main_curr = MAIN_STATE::M_STATE_LANDING_RETURN;
			   }
			}
			break;
		default:

			break;
	} /* end of Bucket ABCD task */
}

void
HelicopterTaskPlan::guide_flag_set(void)
{
	_plan_guide.Pos_En = true;
	_plan_guide.Vel_En = true;
	_plan_guide.PosResetFlag = false;
}
uint8_t
HelicopterTaskPlan::main_state_valid(uint8_t m_state)
{
	if(m_state <= MAIN_STATE::M_STATE_LANDING_LANDING){ /* valid main state */
		return m_state;
	}
	return MAIN_STATE::M_IDLE;
}
uint8_t
HelicopterTaskPlan::sec_state_valid(uint8_t s_state)
{
	if(s_state <= SEC_STATE::S_BUCKET_PUT_RISE){ /* valid main state */
		return s_state;
	}
	return SEC_STATE::S_IDLE;
}
void
HelicopterTaskPlan::global_to_local(double lat, double lon, float alt, math::Vector<3> *curr_sp)
{
	/* _ref_pos must be init before use */
	map_projection_project(&_ref_pos, lat, lon, &curr_sp->data[0], &curr_sp->data[1]);
	curr_sp->data[2] = -(alt - _ref_alt);
}
bool
HelicopterTaskPlan::update_local_pos(void)
{
	bool guide_update = false;
	if (_buckets_valid && _arming && _isLocalBucketsInit) {  /* data is valid */
		/* 1. guide gps update */
//		orb_check(_gps_sia_sub, &guide_update);
//		if (guide_update) {
//			orb_copy(ORB_ID(gps_sia), _gps_sia_sub, &_guide_global_pos);
//			_mission_buckets_pos.copter_lat_guide = math::radians(_guide_global_pos.lat / (double)1e7);
//			_mission_buckets_pos.copter_lon_guide = math::radians(_guide_global_pos.lon / (double)1e7) - M_PI;
//			_mission_buckets_pos.copter_alt_guide = _guide_global_pos.alt / 1e3f;

			global_to_local(_mission_buckets_pos.copter_lat_guide, _mission_buckets_pos.copter_lon_guide,
							_mission_buckets_pos.copter_alt_guide, &_hc_guide);	/* calculate guide's local pos */
			/* 1. pulisher buckets pos */
			/* 2. pos convert into d_pos */
			_bucket_a_curr =  _hc_guide + _d_pos_a_curr;
			_bucket_b_curr =  _hc_guide + _d_pos_b_curr;
			_bucket_c_curr =  _hc_guide + _d_pos_c_curr;
			_bucket_d_curr =  _hc_guide + _d_pos_d_curr;
			_bucket_a_next =  _hc_guide + _d_pos_a_next;
			_bucket_b_next =  _hc_guide + _d_pos_b_next;
			_bucket_c_next =  _hc_guide + _d_pos_c_next;
			_bucket_d_next =  _hc_guide + _d_pos_d_next;
			guide_update = true;
//		}
	}
	return guide_update;
}
void
HelicopterTaskPlan::buckets_local_pos_init(void)
{
	if (_buckets_valid && _arming && !_isLocalBucketsInit) {
		/* 1. convert global to local */
		global_to_local(_mission_buckets_pos.bucketA_lat_curr, _mission_buckets_pos.bucketA_lon_curr,
					  _mission_buckets_pos.bucketA_alt_curr, &_bucket_a_curr);
		global_to_local(_mission_buckets_pos.bucketB_lat_curr, _mission_buckets_pos.bucketB_lon_curr,
					  _mission_buckets_pos.bucketB_alt_curr, &_bucket_b_curr);
		global_to_local(_mission_buckets_pos.bucketC_lat_curr, _mission_buckets_pos.bucketC_lon_curr,
					  _mission_buckets_pos.bucketC_alt_curr, &_bucket_c_curr);
		global_to_local(_mission_buckets_pos.bucketD_lat_curr, _mission_buckets_pos.bucketD_lon_curr,
					  _mission_buckets_pos.bucketD_alt_curr, &_bucket_d_curr);
		global_to_local(_mission_buckets_pos.bucketA_lat_next, _mission_buckets_pos.bucketA_lon_next,
					  _mission_buckets_pos.bucketA_alt_next, &_bucket_a_next);
		global_to_local(_mission_buckets_pos.bucketB_lat_next, _mission_buckets_pos.bucketB_lon_next,
					  _mission_buckets_pos.bucketB_alt_next, &_bucket_b_next);
		global_to_local(_mission_buckets_pos.bucketC_lat_next, _mission_buckets_pos.bucketC_lon_next,
					  _mission_buckets_pos.bucketC_alt_next, &_bucket_c_next);
		global_to_local(_mission_buckets_pos.bucketD_lat_next, _mission_buckets_pos.bucketD_lon_next,
					  _mission_buckets_pos.bucketD_alt_next, &_bucket_d_next);
		global_to_local(_mission_buckets_pos.copter_lat_home, _mission_buckets_pos.copter_lon_home,
					  _mission_buckets_pos.copter_alt_home, &_hc_home);
		global_to_local(_mission_buckets_pos.copter_lat_guide, _mission_buckets_pos.copter_lon_guide,
					  _mission_buckets_pos.copter_alt_guide, &_hc_guide);
		/* 2. pos convert into d_pos */
		_d_pos_a_curr = _bucket_a_curr - _hc_guide;
		_d_pos_b_curr = _bucket_b_curr - _hc_guide;
		_d_pos_c_curr = _bucket_c_curr - _hc_guide;
		_d_pos_d_curr = _bucket_d_curr - _hc_guide;
		_d_pos_a_next = _bucket_a_next - _hc_guide;
		_d_pos_b_next = _bucket_b_next - _hc_guide;
		_d_pos_c_next = _bucket_c_next - _hc_guide;
		_d_pos_d_next = _bucket_d_next - _hc_guide;
		_isLocalBucketsInit = true;
	}
}
void
HelicopterTaskPlan::publish_buckets_pos(void)
{
	if(_buckets_valid){
		_buckets_poll.bucketA_alt_curr = _mission_buckets_pos.bucketA_alt_curr;
		_buckets_poll.bucketA_lat_curr = _mission_buckets_pos.bucketA_lat_curr;
		_buckets_poll.bucketA_lon_curr = _mission_buckets_pos.bucketA_lon_curr;
		_buckets_poll.bucketA_alt_next = _mission_buckets_pos.bucketA_alt_next;
		_buckets_poll.bucketA_lat_next = _mission_buckets_pos.bucketA_lat_next;
		_buckets_poll.bucketA_lon_next = _mission_buckets_pos.bucketA_lon_next;

		_buckets_poll.bucketB_alt_curr = _mission_buckets_pos.bucketB_alt_curr;
		_buckets_poll.bucketB_lat_curr = _mission_buckets_pos.bucketB_lat_curr;
		_buckets_poll.bucketB_lon_curr = _mission_buckets_pos.bucketB_lon_curr;
		_buckets_poll.bucketB_alt_next = _mission_buckets_pos.bucketB_alt_next;
		_buckets_poll.bucketB_lat_next = _mission_buckets_pos.bucketB_lat_next;
		_buckets_poll.bucketB_lon_next = _mission_buckets_pos.bucketB_lon_next;

		_buckets_poll.bucketC_alt_curr = _mission_buckets_pos.bucketC_alt_curr;
		_buckets_poll.bucketC_lat_curr = _mission_buckets_pos.bucketC_lat_curr;
		_buckets_poll.bucketC_lon_curr = _mission_buckets_pos.bucketC_lon_curr;
		_buckets_poll.bucketC_alt_next = _mission_buckets_pos.bucketC_alt_next;
		_buckets_poll.bucketC_lat_next = _mission_buckets_pos.bucketC_lat_next;
		_buckets_poll.bucketC_lon_next = _mission_buckets_pos.bucketC_lon_next;

		_buckets_poll.bucketD_alt_curr = _mission_buckets_pos.bucketD_alt_curr;
		_buckets_poll.bucketD_lat_curr = _mission_buckets_pos.bucketD_lat_curr;
		_buckets_poll.bucketD_lon_curr = _mission_buckets_pos.bucketD_lon_curr;
		_buckets_poll.bucketD_alt_next = _mission_buckets_pos.bucketD_alt_next;
		_buckets_poll.bucketD_lat_next = _mission_buckets_pos.bucketD_lat_next;
		_buckets_poll.bucketD_lon_next = _mission_buckets_pos.bucketD_lon_next;

		_buckets_poll.alt_home = _mission_buckets_pos.copter_alt_home;
		_buckets_poll.lat_home = _mission_buckets_pos.copter_lat_home;
		_buckets_poll.lon_home = _mission_buckets_pos.copter_lon_home;

		_buckets_poll.alt_guide = _mission_buckets_pos.copter_alt_guide;
		_buckets_poll.lat_guide = _mission_buckets_pos.copter_lat_guide;
		_buckets_poll.lon_guide = _mission_buckets_pos.copter_lon_guide;

		/* publish buckets pos to GCS */
		if (_buckets_poll_pub > 0) {
			orb_publish(ORB_ID(hc_buckets_poll), _buckets_poll_pub, &_buckets_poll);
		} else {
			_buckets_poll_pub = orb_advertise(ORB_ID(hc_buckets_poll), &_buckets_poll);
		}
	}
}
