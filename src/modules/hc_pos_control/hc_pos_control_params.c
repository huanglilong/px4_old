/**
 * @file hc_pos_control_params.c
 * Helicopter position controller parameters.
 *
 * @author Anton Babushkin <anton.babushkin@me.com>
 */

#include <systemlib/param/param.h>

/**
 * Minimum thrust
 *
 * Minimum vertical thrust. It's recommended to set it > 0 to avoid free fall with zero thrust.
 *
 * @min 0.0
 * @max 1.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_THR_MIN, 0.1f);

/**
 * Maximum thrust
 *
 * Limit max allowed thrust.
 *
 * @min 0.0
 * @max 1.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_THR_MAX, 1.0f);

/**
 * Proportional gain for vertical position error
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Z_P, 1.0f);
PARAM_DEFINE_FLOAT(HPC_Z_D, 0.0f);
PARAM_DEFINE_FLOAT(HPC_Z_I, 0.0f);
/**
 * Proportional gain for vertical velocity error
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Z_VEL_P, 0.1f);

/**
 * Integral gain for vertical velocity error
 *
 * Non zero value allows hovering thrust estimation on stabilized or autonomous takeoff.
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Z_VEL_I, 0.02f);

/**
 * Differential gain for vertical velocity error
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Z_VEL_D, 0.0f);

/**
 * Maximum vertical velocity
 *
 * Maximum vertical velocity in AUTO mode and endpoint for stabilized modes (ALTCTRL, POSCTRL).
 *
 * @unit m/s
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Z_VEL_MAX, 5.0f);

/**
 * Vertical velocity feed forward
 *
 * Feed forward weight for altitude control in stabilized modes (ALTCTRL, POSCTRL). 0 will give slow responce and no overshot, 1 - fast responce and big overshot.
 *
 * @min 0.0
 * @max 1.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Z_FF, 0.5f);

/**
 * Proportional gain for roll position error
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_X_P, 1.0f);
PARAM_DEFINE_FLOAT(HPC_X_D, 0.0f);
PARAM_DEFINE_FLOAT(HPC_X_I, 0.0f);

/**
 * Proportional gain for roll velocity error
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_X_VEL_P, 0.1f);

/**
 * Integral gain for roll velocity error
 *
 * Non-zero value allows to resist wind.
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_X_VEL_I, 0.02f);

/**
 * Differential gain for roll velocity error. Small values help reduce fast oscillations. If value is too big oscillations will appear again.
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_X_VEL_D, 0.01f);

/**
 * Maximum roll velocity
 *
 * Maximum roll velocity in AUTO mode and endpoint for position stabilized mode (POSCTRL).
 *
 * @unit m/s
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_X_VEL_MAX, 5.0f);

/**
 * roll velocity feed forward
 *
 * Feed forward weight for position control in position control mode (POSCTRL). 0 will give slow responce and no overshot, 1 - fast responce and big overshot.
 *
 * @min 0.0
 * @max 1.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_X_FF, 0.5f);

/**
 * Proportional gain for pitch position error
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Y_P, 1.0f);
PARAM_DEFINE_FLOAT(HPC_Y_D, 0.0f);
PARAM_DEFINE_FLOAT(HPC_Y_I, 0.0f);
/**
 * Proportional gain for pitch velocity error
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Y_VEL_P, 0.1f);

/**
 * Integral gain for pitch velocity error
 *
 * Non-zero value allows to resist wind.
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Y_VEL_I, 0.02f);

/**
 * Differential gain for pitch velocity error. Small values help reduce fast oscillations. If value is too big oscillations will appear again.
 *
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Y_VEL_D, 0.01f);

/**
 * Maximum pitch velocity
 *
 * Maximum pitch velocity in AUTO mode and endpoint for position stabilized mode (POSCTRL).
 *
 * @unit m/s
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Y_VEL_MAX, 5.0f);

/**
 * pitch velocity feed forward
 *
 * Feed forward weight for position control in position control mode (POSCTRL). 0 will give slow responce and no overshot, 1 - fast responce and big overshot.
 *
 * @min 0.0
 * @max 1.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_Y_FF, 0.5f);
/**
 * Maximum tilt angle in air
 *
 * Limits maximum tilt in AUTO and POSCTRL modes during flight.
 *
 * @unit deg
 * @min 0.0
 * @max 90.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_TILTMAX_AIR, 45.0f);

/**
 * Maximum tilt during landing
 *
 * Limits maximum tilt angle on landing.
 *
 * @unit deg
 * @min 0.0
 * @max 90.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_TILTMAX_LND, 15.0f);

/**
 * Landing descend rate
 *
 * @unit m/s
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_LAND_SPEED, 1.0f);

/**
 * Max manual roll
 *
 * @unit deg
 * @min 0.0
 * @max 90.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_MAN_R_MAX, 35.0f);

/**
 * Max manual pitch
 *
 * @unit deg
 * @min 0.0
 * @max 90.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_MAN_P_MAX, 35.0f);

/**
 * Max manual yaw rate
 *
 * @unit deg/s
 * @min 0.0
 * @group Helicopter Position Control
 */
PARAM_DEFINE_FLOAT(HPC_MAN_Y_MAX, 120.0f);

/**
 * Altitude for move_to_setpoint() -- for debug -- 2015/6/25
 *
 * @unit m
 */
PARAM_DEFINE_FLOAT(HPC_ALT_DEBUG, 2.0f);

/**
 * Axis - x,y,z control bias
 * @uint m
 */
PARAM_DEFINE_FLOAT(HPC_BIAS_X, 0.2f);
PARAM_DEFINE_FLOAT(HPC_BIAS_Y, 0.2f);
PARAM_DEFINE_FLOAT(HPC_BIAS_Z, 0.2f);
/**
 * Buckets height -- not use
 * @unit m
 */
PARAM_DEFINE_FLOAT(HPC_BUCKET_HIGH, 4.0f);

/**
 * Handle height
 * @unit m
 */
PARAM_DEFINE_FLOAT(HPC_HANDLE_HIGH, 4.0f);

/**
 * Take off height
 * @uint m
 */
PARAM_DEFINE_FLOAT(HPC_TAKEOFF_HIGH, 1.0f);

/**
 * Bucket cacthed height
 * @unit m
 */
PARAM_DEFINE_FLOAT(HPC_TRACK_HIGH, 3.0f);

/**
 * Buckets supply height
 * @uint m
 */
PARAM_DEFINE_FLOAT(HPC_TRACK_LOW, 2.0f);

/**
 * Bucket catching height
 * @uint m
 */
PARAM_DEFINE_FLOAT(HPC_CATCH_HIGH, 2.0f);

/**
 * Bucket puting height
 * @uint m
 */
PARAM_DEFINE_FLOAT(HPC_PUT_HIGH, 2.0f);

/**
 * Bucket catch try times
 * @uint none
 */
PARAM_DEFINE_FLOAT(HPC_MAX_TRY, 4.0f);
