/**
 * file 	: hc_task_plan.h
 * brief	: Helicopter task planer -- interface
 * author	: huanglilong(huanglilongwk@163.com)
 * time		: 2015/9/19
 * ref		: plan.h
 */
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <lib/mathlib/mathlib.h>
#include <lib/geo/geo.h>
#include <uORB/topics/plan_guidance.h>				/* for pos controller */
#include <uORB/topics/mission_buckets_position.h>
#include <uORB/topics/mission_pos_poll.h>
#include <uORB/topics/mission_start_stop.h>
#include <uORB/topics/vehicle_global_position.h>
#include <uORB/topics/gps_sia.h>
#include <uORB/topics/hc_buckets_poll.h>

/**
 * task plan -- task state machine
 */
class HelicopterTaskPlan{

public:
	HelicopterTaskPlan();					/* constructor */
	~HelicopterTaskPlan();
	bool task_state_machine_GPS(void);		/* state machine -- use gps */
	void poll_subscriptions(void);  		/* poll all data */
	bool update_local_pos(void);			/* calculate local pos after arm */
	void buckets_local_pos_init(void);		/* init d_pos */
	void publish_buckets_pos(void);

	uint8_t _state_main_curr;				/* current main state */
	uint8_t _state_sec_curr;				/* second state for bucket catch */
	bool    _buckets_valid;	    			/* buckets data update */
	int     _catchTryCnts;					/* catch buckets counts */
	float   _dt;							/* hc_pos --> pos controller time -- value update from hc_pos*/
	float   _dt_sum;						/* sum of dt */
	float   _manual_control;				/* hc_pos --> AUX2 -- debug */
	bool	_arming;						/* hc_pos --> arm status */
	bool    _isLocalBucketsInit;			/* hc_pos --> */
	bool    _take_off_flag;					/* hc_pos */

	/* hc_pos --> subscriber */
	int 	_buckets_pos_sub;				/**< buckets pos */
	int		_poll_pos_sub;					/**< 10 pos request */
	int		_start_stop_sub;				/**< stop and start */
	int     _gps_sia_sub;					/**< guide gps data */

	/* publishers */
	orb_advert_t _plan_guidance_pub;		/* publish state of task */
	orb_advert_t _buckets_poll_pub;

	/* hc_pos --> paramters */
	struct{
	   float bias_x;
	   float bias_y;
	   float bias_z;
	   float bucket_height;
	   float handle_height;
	   float takeoff_height;
	   float track_height_high;
	   float track_height_low;
	   float catch_height;
	   float put_height;
	   float hook_delay;
	   int   max_try;
	} _params;

	/* main state */
	struct MAIN_STATE{
		static const uint8_t M_IDLE = 0;
		static const uint8_t M_TAKEOFF_TAKEOFF =1;
		static const uint8_t M_TAKEOFF_HEIGHT_FIX =2;
		static const uint8_t M_STATE_BUCKET_A =3;
		static const uint8_t M_STATE_BUCKET_B =4;
		static const uint8_t M_STATE_BUCKET_C =5;
		static const uint8_t M_STATE_BUCKET_D =6;
		static const uint8_t M_STATE_LANDING_RETURN =7;
		static const uint8_t M_STATE_LANDING_LANDING =8;
		static const uint8_t M_STATE_TSAK_FINISHED =9;
	};
	/* second state */
	struct SEC_STATE{
		static const uint8_t S_IDLE =0;
		static const uint8_t S_BUCKET_TRACK_SUPPLY =1;
		static const uint8_t S_BUCKET_PICK_FALL =2;
		static const uint8_t S_BUCKET_PICK_BUCKET =3;
		static const uint8_t S_BUCKET_PICK_RISE =4;
		static const uint8_t S_BUCKET_TRACK_TARGET =5;
		static const uint8_t S_BUCKET_PUT_FALL =6;
		static const uint8_t S_BUCKET_PUT_DOWN =7;
		static const uint8_t S_BUCKET_PUT_RISE =8;
	};

	struct plan_guidance_s						_plan_guide;  			/* guidance msg for controller */
	struct mission_pos_poll_s					_mission_pos_poll;		/* request pos of buckets */
	struct mission_start_stop_s					_mission_start_stop;	/* start flag of mission */
	struct mission_buckets_position_s			_mission_buckets_pos;	/* pos of buckets from GC */
	struct gps_sia_s							_guide_global_pos;		/* guide pos */
	struct hc_buckets_poll_s                    _buckets_poll;			/* return to GCS */

	struct map_projection_reference_s 			_ref_pos;				/* hc_pos --> pos controller */
	float 										_ref_alt;
	math::Vector<3> 							_pos_curr;				/* hc_pos --> pos controller */

private:
	math::Vector<3> _d_pos_a_curr, _d_pos_b_curr, _d_pos_c_curr, _d_pos_d_curr,
					_d_pos_a_next, _d_pos_b_next, _d_pos_c_next, _d_pos_d_next;

	math::Vector<3> _bucket_a_curr, _bucket_b_curr, _bucket_c_curr, _bucket_d_curr,
	 	 	 	 	_bucket_a_next, _bucket_b_next, _bucket_c_next, _bucket_d_next,
					_hc_home, _hc_guide;
	void    guide_flag_set   (void);
	void    bucket_task      (math::Vector<3> task_pos, math::Vector<3> task_pos_next);
	uint8_t main_state_valid (uint8_t m_state);
	uint8_t sec_state_valid  (uint8_t s_state);
	void    global_to_local  (double lat, double lon, float alt, math::Vector<3> *curr_sp);
};
